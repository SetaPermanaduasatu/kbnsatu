<?php $this->load->view('sdm/assets/head'); ?>

<body>
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
        
        <?php $this->load->view('sdm/assets/sidebar'); ?>
        <!-- main content area start -->
        <div class="main-content">
            <?php $this->load->view('sdm/assets/header'); ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Dashboard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <!-- market value area start -->
                <div class="row mt-5 mb-5">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title">Data Pegawai</h4>
                                <!-- <div class="col-lg-6 mt-5">
                                    <a href="<?php echo site_url('data/pensiun');?>" class="btn btn-primary mb-3 pull-right">Data Pensiun</a>
                                    <a type="button" class="btn btn-flat btn-success mb-3 pull-right" href="#" type="button">Print</a>
                                </div> -->
                                <div class="data-tables datatable-primary">
                                    <table id="dataTable2" class="text-center">
                                        <thead class="text-capitalize">
                                            <tr>
                                                <th>No</th>
                                                <th>NPP</th>
                                                <th>Nama</th>
                                                <th>Tanggal Lahir</th>
                                                <!-- <th>Kelola</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                    $no = 1; 
                    foreach ($pensiun as $data) {
                ?>
                                            <tr>
                                                <td><?php echo $no++ ?></td>
                                                <td><?php echo $data['npp'];?></td>
                                                <td><?php echo $data['nama'];?></td>
                                                <td><?php echo  date('d-m-Y', strtotime($data['tgl_lahir']));?></td>
                                                <!-- <td></td> -->
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- market value area end -->
                <!-- row area start-->
            </div>
        </div>
        <!-- main content area end -->
        <?php $this->load->view('sdm/assets/footer'); ?>
    </div>
    <!-- page container area end -->
    <!-- jquery latest version -->
    <script src="<?php echo base_url();?>assets/sdm/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- bootstrap 4 js -->
    <script src="<?php echo base_url();?>assets/sdm/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/js/metisMenu.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/js/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/js/jquery.slicknav.min.js"></script>

    <script src="<?php echo base_url();?>assets/sdm/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/datatables/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/sdm/datatables/responsive.bootstrap.min.js"></script>
    <!-- start chart js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    start highcharts js
    <script src="https://code.highcharts.com/highcharts.js"></script>
    start zingchart js
    <script src="https://cdn.zingchart.com/zingchart.min.js"></script>
    <script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
    </script>
    <!-- all line chart activation -->
    <script src="<?php echo base_url();?>assets/sdm/js/line-chart.js"></script>
    <!-- all pie chart -->
    <script src="<?php echo base_url();?>assets/sdm/js/pie-chart.js"></script>
    <!-- others plugins -->
    <script src="<?php echo base_url();?>assets/sdm/js/plugins.js"></script>
    <script src="<?php echo base_url();?>assets/sdm/js/scripts.js"></script>
</body>

</html>
