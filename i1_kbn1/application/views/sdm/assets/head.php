<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Pegawai Pensiun</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/kbn.png">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/metisMenu.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/slicknav.min.css">
    <!-- amchart css -->
    <<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sdm/datatables/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sdm/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sdm/datatables/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sdm/datatables/responsive.jqueryui.min.css">
    <!-- others css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/typography.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/default-css.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/sdm/css/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/sdm/css/responsive.css">
    <!-- modernizr css -->
    <script src="<?php echo base_url();?>assets/sdm/js/vendor/modernizr-2.8.3.min.js"></script>
</head>