 <!-- sidebar menu area start -->
        <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="<?php echo base_url()?>index.php/beranda">PT. KBN (PERSERO)</a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li>
                                <a href="<?php echo base_url()?>index.php/beranda" aria-expanded="true"><i class="ti-dashboard"></i><span>Beranda</span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-table"></i>
                                    <span>Unit Kerja</span></a>
                                <ul class="collapse">
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_sekper">Sekretaris Perusahaan</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_spi">Satuan Pengawasan Intern</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_perencanaan">Perencanaan</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_pemasaran">Pemasaran</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_edp">Manajemen Informasi</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_sdm">SDM & Umum</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_ak">Akuntansi & Keuangan</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_pkbl">PKBL</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_mo">Manajemen Operasional</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_keamanan">Keamanan</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_cakung">Cakung</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_marunda">Marunda & Tg. Priok</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_uupl">UUPL</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_air">SBU Air</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_priton">SBU Prima Beton</a></li>
                                    <li><a href="<?php echo base_url()?>index.php/data/ambil_plb">SBU PLB</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->