<?php
 
class M_data extends CI_Model{


	function semua()
	{
    	$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja, tgl_lahir FROM pegawai INNER JOIN jns_jbtan INNER JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id ORDER BY RAND()");
     	return $query->result_array();
	}
	
	function pensiun()
	{
		$query = $this->db->query("SELECT npp, nama, tgl_lahir FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE YEAR(CURDATE()) - YEAR(tgl_lahir) >= 55");
		return $query->result_array();
	}

	function ambil_sekper()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 1");
		return $query->result_array();
	}

	function ambil_spi()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 2");
		return $query->result_array();
	}

	function ambil_perencanaan()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 3");
		return $query->result_array();
	}

	function ambil_pemasaran()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 4");
		return $query->result_array();
	}

	function ambil_edp()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 5");
		return $query->result_array();
	}

	function ambil_sdm()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 6");
		return $query->result_array();
	}

	function ambil_ak()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 7");
		return $query->result_array();
	}

	function ambil_pkbl()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 8");
		return $query->result_array();
	}

	function ambil_mo()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 9");
		return $query->result_array();
	}

	function ambil_keamanan()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 10");
		return $query->result_array();
	}

	function ambil_cakung()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 11");
		return $query->result_array();
	}

	function ambil_marunda()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 12");
		return $query->result_array();
	}

	function ambil_uupl()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 13");
		return $query->result_array();
	}

	function ambil_air()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 14");
		return $query->result_array();
	}

	function ambil_priton()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 15");
		return $query->result_array();
	}

	function ambil_plb()
	{
		$query = $this->db->query("SELECT npp, nama, pendidikan, jns_jbtan, jabatan, unit_kerja FROM pegawai JOIN jns_jbtan JOIN unit_kerja ON pegawai.kd_jns_jbtan = jns_jbtan.kd AND pegawai.id_unit_kerja = unit_kerja.id WHERE id_unit_kerja = 16");
		return $query->result_array();
	}
}