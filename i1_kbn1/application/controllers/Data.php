<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url());
		}
	}

	// public function index()
	// {
	// 	$data['semua'] = $this->m_data->semua();
	// 	$this->load->view('sdm/data_pegawai', $data);
	// }

	function ambil_sekper()
	{
		$data['sekper'] = $this->m_data->ambil_sekper();
		$this->load->view('sdm/unit_kerja/sekper/index', $data);
	}

	function ambil_spi()
	{
		$data['spi'] = $this->m_data->ambil_spi();
		$this->load->view('sdm/unit_kerja/spi/index', $data);
	}

	function ambil_perencanaan()
	{
		$data['perencanaan'] = $this->m_data->ambil_perencanaan();
		$this->load->view('sdm/unit_kerja/perencanaan/index', $data);
	}

	function ambil_pemasaran()
	{
		$data['pemasaran'] = $this->m_data->ambil_pemasaran();
		$this->load->view('sdm/unit_kerja/pemasaran/index', $data);
	}

	function ambil_edp()
	{
		$data['edp'] = $this->m_data->ambil_edp();
		$this->load->view('sdm/unit_kerja/edp/index', $data);
	}

	function ambil_sdm()
	{
		$data['sdm'] = $this->m_data->ambil_sdm();
		$this->load->view('sdm/unit_kerja/sdm_umum/index', $data);
	}

	function ambil_ak()
	{
		$data['ak'] = $this->m_data->ambil_ak();
		$this->load->view('sdm/unit_kerja/akuntansi/index', $data);
	}

	function ambil_pkbl()
	{
		$data['pkbl'] = $this->m_data->ambil_pkbl();
		$this->load->view('sdm/unit_kerja/pkbl/index', $data);
	}

	function ambil_mo()
	{
		$data['mo'] = $this->m_data->ambil_mo();
		$this->load->view('sdm/unit_kerja/manajemen_operasional/index', $data);
	}

	function ambil_keamanan()
	{
		$data['keamanan'] = $this->m_data->ambil_keamanan();
		$this->load->view('sdm/unit_kerja/keamanan/index', $data);
	}

	function ambil_cakung()
	{
		$data['cakung'] = $this->m_data->ambil_cakung();
		$this->load->view('sdm/unit_kerja/cakung/index', $data);
	}

	function ambil_marunda()
	{
		$data['marunda'] = $this->m_data->ambil_marunda();
		$this->load->view('sdm/unit_kerja/marunda/index', $data);
	}

	function ambil_uupl()
	{
		$data['uupl'] = $this->m_data->ambil_uupl();
		$this->load->view('sdm/unit_kerja/uupl/index', $data);
	}

	function ambil_air()
	{
		$data['air'] = $this->m_data->ambil_air();
		$this->load->view('sdm/unit_kerja/sbu_air/index', $data);
	}

	function ambil_priton()
	{
		$data['priton'] = $this->m_data->ambil_priton();
		$this->load->view('sdm/unit_kerja/sbu_priton/index', $data);
	}

	function ambil_plb()
	{
		$data['plb'] = $this->m_data->ambil_plb();
		$this->load->view('sdm/unit_kerja/sbu_plb/index', $data);
	}
}