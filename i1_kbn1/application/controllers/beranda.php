<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	function __construct(){
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url());
		}
	}

	public function index()
	{
		$data['semua'] = $this->m_data->semua();
		// print_r($data2); die;
		$this->load->view('sdm/index', $data);
	}

	function pensiun()
	{
		$data['pensiun'] = $this->m_data->pensiun();
		$this->load->view('sdm/pensiun', $data);
	}
}