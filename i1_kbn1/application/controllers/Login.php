<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('Login/index');
	}

	public function aksi_login(){
	  $username = $this->input->post('username');
    $password	= $this->input->post('password');
    $where		= array(
      'username'	=> $username,
      'password'	=> $password
      );
    $cek = $this->m_login->cek_login("pengguna",$where)->num_rows();
    if($cek > 0) {
      $data_session = array(
        'username'	=> $username,
        'password'	=> $password,
        'status'    => "login"
        );
      $this->session->set_userdata($data_session);
      redirect(base_urL('index.php/beranda'));
    }else{
      echo "Username dan password salah !";
    }
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect(base_url());
  }
}
