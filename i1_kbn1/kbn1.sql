-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Nov 2019 pada 11.48
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kbn1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `jns_jbtan`
--

CREATE TABLE `jns_jbtan` (
  `kd` int(11) NOT NULL,
  `jns_jbtan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jns_jbtan`
--

INSERT INTO `jns_jbtan` (`kd`, `jns_jbtan`) VALUES
(1, 'Kepala Divisi'),
(2, 'Kepala Bagian'),
(3, 'Kepala Seksi'),
(4, 'Pelaksana');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `npp` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kd_jns_jbtan` int(11) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `mulai_kerja` date NOT NULL,
  `tmt_jbtan` date NOT NULL,
  `tmt_unit_kerja` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`npp`, `nama`, `pendidikan`, `tgl_lahir`, `kd_jns_jbtan`, `jabatan`, `id_unit_kerja`, `mulai_kerja`, `tmt_jbtan`, `tmt_unit_kerja`) VALUES
('0027050275', 'Onky Tambunanm, SE., MM', 'S2 Manajemen Sumber Daya Manusia', '1975-02-15', 3, 'Administrasi Manajemen Resiko', 1, '2000-04-01', '2018-10-31', '2013-07-05'),
('006780772', 'ZULKARNAIN DHARSUKI', 'SMKK Boga', '1972-07-30', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1997-12-17', '1997-12-17', '2009-01-06'),
('006801176', 'RUSTONO, SE', 'S1 Manajemen Perusahaan', '1976-11-17', 3, 'Administrasi Keamanan', 10, '1998-09-17', '2012-01-13', '2012-01-13'),
('006810677', 'BUDI TRAPSILO, SE', 'S1 Manajemen Perusahaan', '1977-06-06', 3, 'Supervisor Pelayanan Pelanggan', 11, '1998-09-17', '2018-10-31', '2012-01-16'),
('006820279', 'SUPRIYATNO, SE', 'S1 Manajemen Perusahaan', '1979-02-01', 2, 'Pjs. Manajer Pelayanan Pergudangan', 13, '1998-09-17', '2019-05-10', '2019-05-10'),
('006830876', 'ANDI RUSTANDI, SE', 'S1 Manajemen Perusahaan', '1976-08-20', 3, 'Supervisor Operasional Mekanik & Alat Berat', 13, '1998-09-17', '2007-09-10', '2018-01-19'),
('006841077', 'OCTAVIANUS BERNARD, SE., QIA', 'S1 Akuntansi', '1977-10-20', 3, 'Supervisor Dokumen', 13, '1998-09-17', '2016-11-23', '2018-08-14'),
('006860879', 'BAMBANG WICAKSONO', 'SMU IPS ', '1979-08-20', 3, 'Keamanan Fisik & PMK & Ketertiban Tg.Priok', 12, '1998-09-17', '2016-11-23', '2016-11-23'),
('006871073', 'NURYANA KURNIAWAN, SE', 'S1 Manajemen Perusahaan', '1973-10-09', 4, 'SBU Pelayanan Logistik', 13, '1998-09-17', '1998-09-17', '2011-02-01'),
('006880278', 'Awan', 'SMK Mesin Tenaga', '1978-02-19', 4, 'Sekretariat Perusahaan', 1, '1998-09-17', '1998-09-17', '2017-11-03'),
('006890574', 'DAHONO HERY SAPUTRO', 'SMA Ilmu-Ilmu Biologi', '1974-05-29', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1998-09-17', '1998-09-17', '2019-06-21'),
('006900677', 'PAL GUNTORO, SE', 'S1 Manajemen Perusahaan', '1977-06-09', 3, 'Pjs. Ketertiban', 11, '1998-09-17', '2019-06-21', '2011-04-01'),
('006930679', 'SUGENG DANI ROSMANTO', 'SMU IPS ', '1979-06-10', 4, 'Divisi Keamanan', 12, '1998-09-17', '1998-09-17', '2018-10-29'),
('006940774', 'SUYONO, SE', 'S1 Manajemen Perusahaan', '1974-07-15', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1998-09-17', '1998-09-17', '2018-08-14'),
('006951073', 'ANDRIANO KRISTIAWAN P. S.Kom', 'S1 Teknik Informatika', '1973-10-13', 2, 'Manajer SBU Pengelolaan Air', 14, '1999-04-01', '2011-03-04', '2019-06-21'),
('026971076', 'RUDI HARTONO H. A., SE', 'S1 Manajemen Perusahaan', '1976-10-26', 3, 'Supervisor Penagihan', 12, '2000-04-01', '2018-10-31', '2012-01-16'),
('026980977', 'BIL SEVEN SIBURIAN, SE', 'S1 Manajemen Perusahaan', '1977-09-23', 4, 'SBU Pelayanan Logistik', 13, '2000-04-01', '2000-04-01', '2011-02-01'),
('026990676', 'ALI KISWOYO', 'SMA Ilmu-Ilmu Sosial', '1976-06-21', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2000-04-01', '2000-04-01', '2008-05-12'),
('027000975', 'EDI KARSONO', 'STM Automotif', '1975-09-23', 4, 'SBU Kawasan Cakung', 11, '2000-04-01', '2000-04-01', '2015-12-10'),
('027010577', 'AGUS DARMANTO', 'STM Listrik', '1977-05-01', 4, 'SBU Pengelolaan Air', 14, '2000-04-01', '2000-04-01', '2016-11-23'),
('027081276', 'Reka, SE', 'S1 Akuntansi', '1976-12-21', 2, 'Auditor Madya', 2, '2000-07-03', '2015-12-16', '2018-01-19'),
('047091275', 'HADIAN KUSPRIYANTO', 'STM Mesin Tenaga', '1975-12-04', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2000-04-01', '2000-04-01', '2018-08-14'),
('057101279', 'KOJIRUN, SE', 'S1 Manajemen Perusahaan', '1979-12-18', 4, 'SBU Kawasan Cakung', 11, '2001-06-06', '2001-06-06', '2018-12-26'),
('057110778', 'SUBAGIYO, SKM., MM', 'S2 Manajemen Sumber Daya Manusia', '1978-07-26', 3, 'Supervisor Pelayanan Kesehatan', 11, '2001-06-06', '2018-03-15', '2018-12-26'),
('057140273', 'MOCH. MULYANA', 'D3 Akuntansi', '1973-02-22', 3, 'Supervisor Penagihan', 11, '2004-05-06', '2011-03-04', '2009-05-05'),
('057150279', 'ARIEF MAULANA ALI, SE', 'S1 Akuntansi', '1979-02-25', 2, 'Manajer Akuntansi & Keuangan', 12, '2003-07-01', '2016-11-23', '2010-06-01'),
('057160571', 'INDRA DWIYANTO, ST', 'S1 Teknik Lingkungan', '1971-05-17', 1, 'General Manajer SBU Kawasan', 11, '2003-07-01', '2019-01-04', '2019-07-12'),
('067171075', 'RINANG PANGESTU BRATA, ST', 'S1 Teknik Sipil', '1975-10-15', 2, 'Manajer SBU Prima Beton', 15, '2004-02-03', '2012-10-18', '2019-01-04'),
('077180382', 'Heher, SE', 'S1 Manajemen', '1982-03-17', 3, 'Auditor Muda', 2, '2005-10-03', '2010-06-25', '2015-11-12'),
('097230685', 'EKANTO ZAKARIA, SE', 'S1 Ekonomi Manajemen Transportasi Laut', '1985-06-26', 3, 'Pjs. Supervisor Operasional', 12, '2007-04-02', '2019-06-21', '2012-01-16'),
('097240867', 'dr. AL FAIRUZABADI', 'S1 Kedokteran Umum', '1967-08-17', 4, 'SBU Kawasan Cakung', 11, '2001-09-01', '2001-09-01', '2018-12-26'),
('097251065', 'dr. EMPAT PATONAH HAMID, MM.Kes', 'S2 Manajemen Rumah Sakit', '1965-10-11', 2, 'Pejabat Setingkat', 11, '2001-10-01', '2001-10-01', '2018-12-26'),
('097260967', 'R. UCU NURCAHYA, ST', 'S1 Teknik Sipil', '1967-09-27', 2, 'Manajer Pemeliharaan Kawasan', 12, '2001-11-01', '2015-12-16', '2018-07-31'),
('107280678', 'Diwan, ST', 'S1 Teknik Elektro', '1978-06-30', 3, 'Pengawasan Sarana / Prasarana', 3, '2008-08-01', '2016-11-23', '2016-11-23'),
('107300877', 'Musir, SE', 'S1 Manajemen Keuangan & Perbankan', '1977-08-24', 3, 'Utilisasi Aset', 4, '2008-08-13', '2013-07-05', '2008-08-13'),
('107310881', 'DIAN SUFITRI, SE', 'S1 Akuntansi', '1981-08-19', 3, 'Supervisor Akuntansi', 11, '2008-08-13', '2016-11-23', '2008-08-13'),
('107320779', 'Yuwan, SE', 'S1 Akuntansi', '1979-07-18', 3, 'Administrasi Penjualan', 4, '2008-08-13', '2013-07-05', '2008-08-13'),
('107331279', 'Yuyat. ST', 'S1 Teknik Elektro', '1979-12-09', 3, 'Perencanaan Sarana / Prasarana', 3, '2008-08-01', '2016-11-23', '2012-09-05'),
('107340986', 'NAROS DIYANINGSIH, SE', 'S1 Akuntansi', '1986-09-09', 3, 'Akuntansi', 7, '2008-08-13', '2016-11-23', '2016-11-23'),
('107350483', 'YANTI APRIANTI, SE', 'S1 Akuntansi', '1983-04-02', 3, 'Supervisor Akuntansi & Keuangan', 14, '2008-08-13', '2013-07-05', '2013-07-05'),
('117361080', 'SETIARSIH, SE', 'S1 Manajemen', '1980-10-01', 2, 'Pjs. Manajer Umum & Personalia', 13, '2008-08-13', '2019-05-10', '2019-05-10'),
('117370879', 'Fani, SE', 'S1 Manajemen Perusahaan', '1979-08-13', 3, 'Penjualan & Promosi', 4, '2008-08-13', '2018-10-31', '2008-08-13'),
('117391175', 'SUSILO', 'SMEA Perdagangan', '1975-11-11', 4, 'SBU Pelayanan Logistik', 13, '2009-04-01', '2009-04-01', '2018-10-29'),
('117400977', 'YUNUS', 'SMA Ilmu-Ilmu Fisik', '1977-09-02', 4, 'SBU Pelayanan Logistik', 13, '2009-04-01', '2009-04-01', '2018-10-29'),
('117410173', 'WARNO', 'SMA IPS', '1973-01-16', 4, 'SBU Pelayanan Logistik', 13, '2009-04-01', '2009-04-01', '2018-10-29'),
('117420167', 'drg. LILIS SUHAENI HIDAYAT', 'S1 Kedokteran Gigi', '1967-01-21', 4, 'SBU Kawasan Cakung', 11, '2000-09-01', '2000-09-01', '2018-12-26'),
('117430867', 'drg. RASDIANI', 'S1 Ilmu Kedokteran Gigi', '1967-08-10', 4, 'SBU Kawasan Cakung', 11, '2006-07-03', '2006-07-03', '2018-12-26'),
('117440975', 'YOPPY HANDRIANTO, SE', 'S1 Ekonomi Manajemen Perusahaan', '1975-09-24', 3, 'Supervisor Pengusahaan Gudang Terbuka', 13, '2010-02-01', '2016-11-23', '2012-01-16'),
('117451278', 'Dewan, SE', 'S1 Ekonomi Manajemen Perusahaan', '1978-12-26', 3, 'Pjs. Auditor Muda', 2, '2010-02-01', '2019-06-21', '2018-10-29'),
('117460980', 'RIZKY WALIADI, SE', 'S1 Ekonomi Manajemen Perusahaan', '1980-09-21', 4, 'SBU Pusat Logistik Berikat', 16, '2010-02-01', '2010-02-01', '2019-04-16'),
('117470981', 'BOEDHI SANTOSO, S.Kom', 'S1 Ilmu Komputer', '1981-09-05', 4, 'Manajemen Operasional', 9, '2010-02-01', '2010-02-01', '2017-11-03'),
('117481281', 'ASQARINI, S.Si', 'S1 Matematika', '1981-12-18', 4, 'Sumber Daya Manusia & Umum', 6, '2010-02-01', '2010-02-01', '2018-08-14'),
('117490284', 'RETNO ALFITRIANDARI, ST', 'S1 Teknik Arsitektur', '1984-02-25', 4, 'SBU Kawasan Cakung', 11, '2010-02-01', '2010-02-01', '2018-10-29'),
('117510587', 'BAGOES RIYADI KURNIAWAN, SE', 'S1 Akuntansi', '1987-05-23', 3, 'Pejabat Setingkat', 9, '2010-02-01', '2018-10-31', '2016-11-23'),
('117530887', 'SAMUEL NICOLAUS P. N, SE', 'S1 Akuntansi', '1987-08-12', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2010-02-01', '2010-02-01', '2010-02-01'),
('117541287', 'DWI YARZUKI FADHILA, SE', 'S1 Ekonomi Manajemen Transportasi Laut', '1987-12-26', 3, 'Supervisor Operasional', 16, '2010-02-01', '2018-10-31', '2018-10-31'),
('117550280', 'Awi, ST., MT', 'S2 Teknik Sipil', '1980-02-10', 2, 'Pengembangan Bisnis', 3, '2010-10-01', '2019-01-04', '2010-10-01'),
('117560580', 'ANDI SIMANJUNTAK, S.Pd', 'S1 Pendidikan Bahasa Inggris', '1980-05-17', 4, 'SBU Pelayanan Logistik', 13, '2010-10-01', '2010-10-01', '2010-10-01'),
('117580781', 'Nuras, ST', 'S1 Teknik Sipil', '1981-07-17', 4, 'Satuan Pengawasan Intern', 2, '2010-10-01', '2010-10-01', '2019-07-12'),
('117600484', 'Mawa, SE', 'S1 Ilmu Komunikasi Hubungan Masyarakat', '1984-04-07', 3, 'Sekretariat', 1, '2010-10-01', '2019-06-21', '2010-10-01'),
('117610485', 'Michael Elveren Nahak, SE', 'S1 Manajemen Keuangan & Perbankan', '1985-04-11', 3, 'Humas', 1, '2010-10-01', '2018-01-19', '2015-12-10'),
('117620685', 'KHARISMAWATI, SE', 'S1 Akuntansi', '1985-06-30', 4, 'SBU Pelayanan Logistik', 13, '2010-10-01', '2010-10-01', '2010-10-01'),
('117631085', 'SAFARINA WIDARTI, SE', 'S1 Akuntansi', '1985-10-18', 4, 'SBU Kawasan Cakung', 11, '2010-10-01', '2010-10-01', '2019-07-12'),
('117640887', 'VENNY DWI LESTARI, SE', 'S1 Akuntansi', '1987-08-11', 4, 'SBU Kawasan Cakung', 11, '2010-10-01', '2010-10-01', '2010-10-01'),
('117650188', 'Maro', 'D3 Sekretaris', '1988-01-29', 3, 'Pejabat', 1, '2010-10-01', '2019-06-21', '2010-10-01'),
('127660573', 'IKAH KARTIKA, SE', 'S1 Manajemen SDM', '1973-05-17', 4, 'SBU Kawasan Cakung', 11, '2011-07-11', '2011-07-11', '2018-10-29'),
('127670674', 'ARMI SANTIA DEWI IHSAN, S.Farm., Apt.', 'S1 Farmasi Apoteker', '1974-06-05', 4, 'SBU Kawasan Cakung', 11, '2011-07-11', '2011-07-11', '2018-12-26'),
('127680383', 'KURNIAWATY, S.Psi.', 'S1 Psikologi', '1983-03-20', 3, 'Pengembangan SDM', 6, '2011-07-11', '2018-01-19', '2013-02-12'),
('127690288', 'Ani, S.S', 'S1 Bahasa Sastra Inggris', '1988-02-12', 4, 'Sekretariat Perusahaan', 1, '2011-07-11', '2011-07-11', '2016-04-01'),
('127700388', 'Desi, SE', 'S1 Ekonomi Manajemen Keuangan & Perbankan', '1988-03-07', 4, 'Sekretariat Perusahaan', 1, '2011-07-11', '2011-07-11', '2019-08-22'),
('137771086', 'RENY AFDIAH, SE', 'S1 Akuntansi', '1986-10-10', 4, 'SBU Kawasan Cakung', 11, '2013-01-02', '2013-01-02', '2013-01-02'),
('137781287', 'Puta, SE', 'S1 Manajemen Perusahaan', '1987-12-04', 4, 'Pemasaran & Pelayanan', 4, '2013-01-02', '2013-01-02', '2015-10-15'),
('137790485', 'FICKY AURICO, SE', 'S1 Manajemen', '1985-04-28', 4, 'SBU Pengelolaan Air', 14, '2013-01-02', '2013-01-02', '2013-01-02'),
('137800988', 'Kyhahap, SE', 'S1 Manajemen Transportasi Laut', '1988-09-15', 4, 'Pemasaran & Pelayanan', 4, '2013-01-02', '2013-01-02', '2016-11-23'),
('137820391', 'Dyas, S.Kom', 'S1 Ilmu Komunikasi', '1991-03-15', 3, 'Pjs. Pelayanan Pelanggan', 4, '2013-01-02', '2019-06-21', '2019-06-21'),
('137831089', 'ADJI DARMAWAN, S.Kom', 'S1 Teknik Informatika', '1989-10-30', 4, 'Bagian Sistem Manajemen Informasi', 5, '2013-01-02', '2013-01-02', '2013-01-02'),
('137840789', 'Nukiyan, ST', 'S1 Teknik Arsitektur', '1989-07-10', 4, 'Perencanaan & Pengawasan', 3, '2013-01-02', '2013-01-02', '2013-01-02'),
('137850886', 'ANDI NURRACHMAN', 'D3 Teknik Elektro', '1986-08-18', 4, 'SBU Kawasan Cakung', 11, '2013-01-02', '2013-01-02', '2013-01-02'),
('137861085', 'Ibrahar, S.Pi., M.Si', 'S2 Magister Sains Biomanajemen', '1985-10-31', 4, 'Perencanaan & Pengawasan', 3, '2013-01-02', '2013-01-02', '2018-08-14'),
('137870885', 'Nana, SE', 'S1 Akuntansi', '1985-08-09', 4, 'Satuan Pengawasan Intern', 2, '2013-01-02', '2013-01-02', '2018-10-29'),
('137881284', 'Fiosa, ST., MM', 'S2 Manajemen Pemasaran', '1984-12-18', 3, 'Pengawasan AMDAL & IPAL', 3, '2013-01-02', '2018-01-19', '2018-01-19'),
('137930585', 'RIKI MOCHAMAD RIZKI, SE', 'S1 Manajemen Perusahaan', '1985-05-04', 3, 'Pjs. Supervisor Akuntansi', 12, '2013-07-01', '2019-06-21', '2013-07-01'),
('137940291', 'HANNA RAHMITASARI, SE', 'S1 Akuntansi', '1991-02-03', 4, 'Program Kemitraan & Bina Lingkungan', 8, '2013-07-01', '2013-07-01', '2019-02-12'),
('137950290', 'ISREZA FEBRIONALDY EKA SAPUTRA, SE', 'S1 Akuntansi', '1990-02-16', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('137960281', 'Abon, ST., MT', 'S2 Teknik Elektro', '1981-02-22', 4, 'Perencanaan & Pengawasan', 3, '2013-07-01', '2013-07-01', '2018-10-29'),
('137970982', 'Fial', 'D3 Transportasi Laut & Kepelabuhanan', '1982-09-24', 4, 'Perencanaan & Pengawasan', 3, '2013-07-01', '2013-07-01', '2014-01-02'),
('137981087', 'ARIS MUNANDAR, ST', 'S1 Teknik Sipil', '1987-10-15', 4, 'SBU Prima Beton', 15, '2013-07-01', '2013-07-01', '2018-10-29'),
('138000974', 'Rafiya, ST., MT', 'S2 Teknik Industri', '1974-09-01', 3, 'Pejabat Setingkat', 3, '2013-04-22', '2018-10-31', '2018-10-31'),
('138010282', 'MUSTOFA', 'SMK Perkantoran', '1982-02-15', 4, 'Sumber Daya Manusia & Umum', 6, '2012-11-05', '2012-11-05', '2012-11-05'),
('138020378', 'HADI SUTIKNO', 'SMK Akuntansi', '1978-03-15', 4, 'SBU Kawasan Cakung', 11, '2012-11-05', '2012-11-05', '2018-10-29'),
('138031267', 'SLAMET RIYADI', 'SMP', '1967-12-31', 4, 'Sumber Daya Manusia & Umum', 6, '2012-11-05', '2012-11-05', '2012-11-05'),
('138040879', 'SUPRIYANTO', 'SMU Ilmu Pengetahuan Sosial', '1979-08-10', 4, 'Sumber Daya Manusia & Umum', 6, '2013-01-16', '2013-01-16', '2014-01-01'),
('138051083', 'VIVI OKTAPIONI', 'D3 Sekretari', '1983-10-31', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138070389', 'Donny Dongoran, SE', 'S1 Manajemen Perusahaan', '1989-03-09', 3, 'Pejabat', 1, '2013-02-01', '2018-10-31', '2013-02-01'),
('138080889', 'RATIH SAYEKTHI, SE', 'S1 Akuntansi', '1989-08-14', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2019-02-12'),
('138100587', 'SYAHRIL SUHANDA, S.Kom', 'S1 Sistem Informasi', '1987-05-26', 4, 'SBU Kawasan Cakung', 11, '2013-02-01', '2013-02-01', '2013-02-01'),
('138111189', 'NOVA ISTANTO, SE', 'S1 Manajemen Logistik & Material', '1989-11-05', 3, 'Supervisor Pemasaran', 16, '2013-02-01', '2018-10-31', '2018-08-14'),
('138121286', 'RYAN CIPTA KUSUMA, SE., MBA', 'S2 Magister Manajemen', '1986-12-06', 3, 'Anggaran', 7, '2013-02-01', '2018-10-31', '2013-02-01'),
('138130689', 'YANA WULANDARI, SE', 'S1 Akuntansi', '1989-06-29', 4, 'Akuntansi & Keuangan', 7, '2013-02-01', '2013-02-01', '2013-02-01'),
('138140586', 'dr. BEBBY RAMAWATI', 'S1 Kedokteran Umum', '1986-05-21', 4, 'SBU Kawasan Cakung', 11, '2013-02-01', '2013-02-01', '2018-12-26'),
('138160491', 'Sipa', 'SMA Ilmu Pengetahuan Sosial', '1991-04-08', 4, 'Sekretariat Perusahaan', 1, '2013-02-01', '2013-02-01', '2019-03-29'),
('138171081', 'MAHFUZ', 'SMK Teknik Mesin Perkakas', '1981-10-03', 4, 'Sumber Daya Manusia & Umum', 6, '2013-02-01', '2013-02-01', '2016-11-23'),
('138180976', 'SUTARMAK', 'D3 Refraksi Optisi', '1976-09-14', 4, 'SBU Kawasan Cakung', 11, '2013-02-01', '2013-02-01', '2018-12-26'),
('138190783', 'ADAY SUDRAJAT', 'SMU Ilmu Pengetahuan Sosial', '1983-07-26', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138200782', 'MUKRON, SE', 'S1 Manajemen Perusahaan', '1982-07-13', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-10-29'),
('138210386', 'HARTANTO, SE', 'S1 Manajemen Perusahaan', '1986-03-10', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2017-11-03'),
('138230787', 'AGUNG FERDIANTO, S.Kom', 'S1 Sistem Informasi', '1987-07-20', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138240183', 'RAYMOND YACOB', 'SMU Ilmu Pengetahuan Sosial', '1983-01-11', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138251180', 'MULYADI', 'SMK Otomotif', '1980-11-27', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138260382', 'ARIE ARDIANSYAH, SE', 'S1 Manajemen Perusahaan', '1982-03-14', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138271092', 'TAUFIK HIDAYAT', 'D3 Manajemen Informatika', '1992-10-07', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138290377', 'RIKHY HIDAYAT', 'SMA Ilmu-Ilmu Sosial', '1977-03-15', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138300183', 'FAJAR BRAMANTYO, SE', 'S1 Manajemen Pemasaran', '1983-01-04', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138311280', 'DEFFI DONALD YUSTHA', 'SMK Listrik Instalasi', '1980-12-16', 4, 'SBU Pusat Logistik Berikat', 16, '2013-02-01', '2013-02-01', '2018-10-29'),
('138320683', 'EKO YUDI RAHMAN', 'SMK Mekanik Umum', '1983-06-29', 4, 'Sumber Daya Manusia & Umum', 6, '2013-02-01', '2013-02-01', '2019-06-21'),
('138330881', 'SUPARMANTO, SE', 'S1 Manajemen Perusahaan', '1981-08-06', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138341272', 'KOMARUDIN', 'SPM Pelayaran', '1972-12-17', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138350183', 'ADANG SAPUTRA', 'SMK Pembuatan & Perbaikan Mesin Kapal', '1983-01-16', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138360377', 'EDY MART L. TOBING', 'SMK Mesin Tenaga', '1977-03-09', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2018-10-29'),
('138371187', 'ROBELLY SIMANUNGKALIT', 'D3 Teknik Sipil', '1987-11-13', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138380981', 'MUSLIMIN', 'SMU Ilmu Pengetahuan Sosial', '1981-09-05', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138390587', 'PANGASIAN SIMANJUNTAK', 'SMA Ilmu Pengetahuan Sosial', '1987-05-03', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138400484', 'AHMAD FAUZI', 'SMK Teknik Mesin Perkakas', '1984-04-18', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138431187', 'ADE MULYANA', 'SD', '1987-11-17', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138440472', 'SURIPTO', 'SD Tidak Berijazah', '1972-04-06', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138451269', 'SOBAR', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1969-12-24', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2013-02-01'),
('138470870', 'AGUS SUNARNO', 'SMP', '1970-08-08', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2014-01-01'),
('138481085', 'LUKMAN BUDIAWAN', 'SMK', '1985-10-08', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138490878', 'M. RASID AL KHOLIQ', 'SMA Ilmu Pengetahuan Alam', '1978-08-18', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138500789', 'Moersyad, ST., MM', 'S2 Manajemen Pemasaran', '1989-07-28', 3, 'Pjs. Perencanaan Bangunan', 3, '2013-02-01', '2019-06-21', '2018-10-29'),
('138510885', 'Liamtang, SE', 'S1 Akuntansi', '1985-08-25', 4, 'Perencanaan & Pengawasan', 3, '2013-02-01', '2013-02-01', '2019-04-16'),
('138520485', 'BAMBANG WIRANTO ADY', 'SMU Ilmu Pengetahuan Sosial', '1982-04-28', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138530689', 'YUNIRMA SARI, S.Hut', 'S1 Tehnologi Hasil Hutan', '1989-06-09', 3, 'Pjs. Administrasi Kepegawaian', 6, '2013-02-01', '2019-06-21', '2018-10-29'),
('138540482', 'EKO NURHIDAYAT', 'SMK Mekanik Otomotif', '1982-04-09', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138550883', 'ARIES HARTANTO', 'SMK', '1983-08-14', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138560584', 'ABDUL AZIZ', 'Madrasah Aliyah Ilmu Pengetahuan Sosial', '1984-05-15', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138570477', 'DWINANTO', 'SMU Ilmu Pengetahuan Sosial', '1977-04-17', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138581186', 'SYAIFULLAH', 'Madrasah Aliyah Ilmu Pengetahuan Alam', '1986-11-15', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138590772', 'JAJULI', 'SMA Ilmu-Ilmu Sosial', '1972-07-24', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138600882', 'Matsur', 'SMK Teknik Mesin', '1982-08-11', 4, 'Sekretariat Perusahaan', 1, '2013-02-01', '2013-02-01', '2018-10-29'),
('138610990', 'JAMILUDIN', 'SMA Ilmu Pengetahuan Sosial', '1990-09-19', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138630288', 'SYAMSUL BACHRI', 'SMK Teknik Mesin', '1988-02-21', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138650991', 'WILLIAM RAJAGUKGUK', 'SMA Ilmu Pengetahuan Sosial', '1991-09-11', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138670378', 'RONI PASLAH', 'SMA Ilmu Pengetahuan Alam', '1978-03-19', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138680386', 'MUHAMMAD AKMALUDDIN, ST', 'S1 Teknik Sipil', '1986-03-29', 3, 'Pjs. Supervisor Administrasi & Umum', 12, '2013-02-01', '2019-06-21', '2013-02-01'),
('138691287', 'PANDAN WANGI, SE', 'S1 Akuntansi', '1987-12-22', 4, 'SBU Pengelolaan Air', 14, '2013-02-01', '2013-02-01', '2013-02-01'),
('138701182', 'MOHAMAD ALIMAHRI, SE', 'S1 Manajemen Perusahaan', '1982-11-11', 4, 'Sumber Daya Manusia & Umum', 6, '2013-02-01', '2013-02-01', '2018-08-14'),
('138710986', 'HENDIMAK MALAU', 'SMK Teknik Mekanik Otomotif', '1986-09-22', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138720584', 'CAPAN', 'SD Tidak Berijazah', '1984-05-05', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138730686', 'ALOYSIUS DARMANTO ADI UTOMO', 'SMK Teknik Mekanik Otomotif', '1986-06-28', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138740777', 'YANURI TUNDO MARSUDI', 'SMK Mesin Tenaga', '1977-07-27', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138751276', 'WAHYU SUWARNO', 'STM Bangunan', '1976-12-25', 4, 'SBU Pengelolaan Air', 14, '2013-02-01', '2013-02-01', '2016-11-23'),
('138760481', 'SUGIARTO', 'SMK Mekanik Otomotif', '1981-04-14', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138780181', 'YUSTINA KURNIAWAN', 'SMK Otomotif', '1981-01-28', 4, 'SBU Pelayanan Logistik', 13, '2013-02-01', '2013-02-01', '2018-10-29'),
('138810391', 'Atofa, ST', 'S1 Teknik Sipil', '1991-03-26', 4, 'Perencanaan & Pengawasan', 3, '2013-02-01', '2013-02-01', '2018-10-29'),
('138820273', 'WIDODO HARYANTO', 'D3 Teknik Sipil', '1973-02-10', 3, 'Supervisor Administrasi & Umum', 15, '2013-02-01', '2018-10-31', '2013-02-01'),
('138840985', 'SURYANTO, S.Pd.I', 'S1 Tadris Matematika', '1985-09-12', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2013-02-01'),
('138850294', 'LUTFI NAVY PEBRIYANTO', 'SMK Teknik Otomotif Kendaraan Ringan', '1994-02-02', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2013-02-01'),
('138871280', 'HOTMAN SIPAHUTAR', 'D3 Teknik Mesin', '1980-12-03', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2017-11-03'),
('138880479', 'ALWI SUDJARWADI', 'SMU Ilmu Pengetahuan Sosial', '1979-04-18', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2013-02-01'),
('138890870', 'DEDI KURNIAWAN', 'SMP', '1970-08-19', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2013-02-01'),
('138950477', 'AHMAD REBI', 'SMEA Perdagangan', '1977-04-19', 4, 'SBU Prima Beton', 15, '2013-02-01', '2013-02-01', '2013-02-01'),
('138960684', 'IKHSAN RAMADHAN, SE', 'S1 Manajemen Perusahaan', '1984-06-10', 4, 'Pemasaran & Pelayanan', 4, '2013-02-01', '2013-02-01', '2015-10-07'),
('138981080', 'SUDIRJO', 'SMA Ilmu Pengetahuan Alam', '1980-10-06', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2013-02-01'),
('138990182', 'ANTON SUJARWO', 'SMA Ilmu Pengetahuan Alam', '1982-01-28', 4, 'SBU Pusat Logistik Berikat', 16, '2013-02-01', '2013-02-01', '2019-04-16'),
('139000384', 'YUDDY MARTASA', 'SMU Ilmu Pengetahuan Sosial', '1984-03-14', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-02-01', '2013-02-01', '2019-04-16'),
('139010981', 'Aho, SE', 'S1 Manajemen Transpor Laut', '1981-09-06', 4, 'Pemasaran & Pelayanan', 4, '2013-02-01', '2013-02-01', '2015-10-07'),
('139020487', 'dr. YETTI MUTHIAH', 'S1 Pendidikan Dokter', '1987-04-17', 3, 'Pejabat Setingkat', 9, '2013-04-01', '2018-10-31', '2013-04-01'),
('139040581', 'MARTINUS HERTANTO, SE', 'S1 Manajemen Perusahaan', '1981-05-09', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-07-01', '2013-07-01', '2013-07-01'),
('139060870', 'RAMLI', 'SMU Ilmu Pengetahuan Sosial', '1970-08-30', 4, 'Sumber Daya Manusia & Umum', 6, '2013-07-01', '2013-07-01', '2013-01-16'),
('139070771', 'NAMIN', 'SD', '1971-07-06', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-07-01', '2013-07-01', '2018-08-14'),
('139080685', 'Tisaqi, SE', 'S1 Manajemen', '1985-06-26', 4, 'Perencanaan & Pengawasan', 3, '2013-07-01', '2013-07-01', '2013-07-01'),
('139090782', 'TRI SETIAWAN YULIANTO, SE', 'S1 Manajemen Perusahaan', '1982-07-24', 4, 'SBU Prima Beton', 15, '2013-07-01', '2013-07-01', '2013-07-01'),
('139100387', 'PALUPI BUDI RAHAYU, SE', 'S1 Akuntansi', '1987-03-22', 4, 'Sumber Daya Manusia & Umum', 6, '2013-07-01', '2013-07-01', '2018-10-29'),
('139110188', 'RULLY RAMADHAN, SE', 'S1 Akuntansi', '1988-01-14', 4, 'Program Kemitraan & Bina Lingkungan', 8, '2013-07-01', '2013-07-01', '2018-10-29'),
('139121290', 'DESY DAME NATALYA, SH', 'S1 Ilmu Hukum', '1990-12-20', 3, 'Pjs. Supervisor Pelayanan Pelanggan', 12, '2013-07-01', '2019-06-21', '2019-06-21'),
('139130968', 'ANWAR', 'SMA Ilmu Pengetahuan Sosial', '1968-09-09', 4, 'Keamanan', 10, '2013-07-01', '2013-07-01', '2013-07-01'),
('139140680', 'MUSTOPA', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1980-06-09', 4, 'Sumber Daya Manusia & Umum', 6, '2013-07-01', '2013-07-01', '2013-07-01'),
('139151082', 'AMDANI', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1982-10-20', 4, 'SBU Pusat Logistik Berikat', 16, '2013-07-01', '2013-07-01', '2018-08-14'),
('139170879', 'AGUS SETIAWAN, ST', 'S1 Teknik Mesin', '1979-08-07', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-07-01', '2013-07-01', '2019-04-16'),
('139180388', 'DHARMAWAN DJUMAING, ST', 'S1 Teknik Industri', '1988-03-17', 4, 'Sumber Daya Manusia & Umum', 6, '2013-07-01', '2013-07-01', '2012-11-05'),
('139191295', 'MUCHAMAD FAJAR', 'SMK Teknik Mesin', '1995-12-08', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('139200895', 'DWI ANDRI LESTIANTO', 'SMK Teknik Instalasi Tenaga Listrik', '1995-08-14', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('139220189', 'SYAIFUL ULUM, S.Pd.I', 'S1 Pendidikan Agama Islam', '1989-01-12', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('139231291', 'HANDIKA FAJAR UTAMA, SE', 'S1 Manajemen Transpor Laut', '1991-12-17', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('139240694', 'DEDI KURNIAWAN', 'SMK Teknik Otomotif Kendaraan Ringan', '1994-06-02', 4, 'SBU Kawasan Cakung', 11, '2013-07-01', '2013-07-01', '2013-07-01'),
('139270481', 'Hulam, ST., MT', 'S1 Teknik Sipil', '1981-04-03', 4, 'Perencanaan & Pengawasan', 3, '2013-09-10', '2013-09-10', '2013-09-10'),
('139291084', 'RACHMAT SANTOSO', 'SMK Teknik Listrik Pemakaian', '1984-10-29', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-09-01', '2013-09-01', '2013-09-01'),
('139301189', 'INNOV IBRAHIM', 'SMA Ilmu Pengetahuan Sosial', '1989-11-06', 4, 'Sumber Daya Manusia & Umum', 6, '2013-09-01', '2013-09-01', '2016-11-23'),
('139311192', 'CHARLIE LESMANA LUMBAN GAOL', 'SMK Administrasi Perkantoran', '1992-11-10', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-10-01', '2013-10-01', '2013-10-01'),
('139320993', 'ALI SYAIFUDIN ', 'SMK Teknik Elektro Industri', '1993-09-29', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-10-01', '2013-10-01', '2013-10-01'),
('139330781', 'UMAR NURJAMAN', 'D3 Teknik Mesin', '1981-07-28', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2013-10-01', '2013-10-01', '2013-10-01'),
('139351289', 'FREDDY JOHANES MANALU, SE', 'S1 Akuntansi', '1989-12-01', 4, 'SBU Pelayanan Logistik', 13, '2013-10-01', '2013-10-01', '2013-10-01'),
('139361189', 'DENNY GINANJAR, SE', 'S1 Manajemen Perusahaan', '1989-11-20', 4, 'SBU Pelayanan Logistik', 13, '2013-10-01', '2013-10-01', '2013-10-01'),
('1410000674', 'ARAFIK DINARTO', 'SMP', '1974-06-19', 4, 'Sumber Daya Manusia & Umum', 6, '2007-06-07', '2007-06-07', '2014-01-01'),
('1410010375', 'DADANG ASMURI', 'SMP (Paket B)', '1975-03-05', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2017-04-28'),
('1410040468', 'SAYUTIH', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1968-04-16', 4, 'SBU Pusat Logistik Berikat', 16, '2003-03-25', '2003-03-25', '2019-07-12'),
('1410050171', 'MADINAH', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1971-01-21', 4, 'Sumber Daya Manusia & Umum', 6, '2003-03-25', '2003-03-25', '2019-07-12'),
('1410070769', 'SUKANTA', 'SMP (Paket B)', '1969-07-24', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410081071', 'MUHAMAD NUR', 'SMP', '1971-10-30', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410090673', 'MOH. HANAFI', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1973-06-14', 4, 'SBU Pengelolaan Air', 14, '2003-03-25', '2003-03-25', '2018-02-15'),
('1410100774', 'SURIYANTA', 'ST Teknik Logam', '1974-07-22', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2003-03-25', '2003-03-25', '2019-04-16'),
('1410110673', 'MUHAMAD KOSIM', 'SMP', '1973-06-21', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2019-04-16'),
('1410120465', 'ABDUL RACHMAN HB.', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1965-04-26', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2018-08-14'),
('1410140370', 'ENDANG PURNOMO', 'SD', '1970-03-27', 4, 'Divisi Keamanan', 11, '2003-03-25', '2003-03-25', '2017-04-28'),
('1410150564', 'MANSYUR', 'SD', '1964-05-13', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2017-04-28'),
('1410160471', 'SAEMAN', 'SD (Paket A)', '1971-04-22', 4, 'Sumber Daya Manusia & Umum', 6, '2003-03-25', '2003-03-25', '2018-03-15'),
('1410170372', 'HERMANTO', 'SD', '1972-03-21', 4, 'Manajemen Operasional', 9, '2003-03-25', '2003-03-25', '2018-08-14'),
('1410191164', 'AGUS SARIPUDIN', 'SD', '1964-11-13', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2019-04-16'),
('1410201268', 'INDRAJIT MAHAMERU', 'SD', '1968-12-10', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410211067', 'ISMAN', 'SD', '1967-10-27', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410220781', 'TARDI', 'SD', '1981-07-20', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410230775', 'RASCA', 'SD', '1975-07-09', 4, 'SBU Pusat Logistik Berikat', 16, '2003-03-25', '2003-03-25', '2019-02-12'),
('1410240877', 'MATJENIH', 'SD (Paket A)', '1977-08-21', 4, 'Sumber Daya Manusia & Umum', 6, '2007-06-07', '2007-06-07', '2013-07-01'),
('1410250777', 'TAMRIN', 'SD', '1977-07-28', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2013-02-01'),
('1410260482', 'ABDI SUNARYA', 'SD (Paket A)', '1982-04-15', 4, 'SBU Pengelolaan Air', 14, '2003-03-25', '2003-03-25', '2018-02-15'),
('1410270664', 'RADAM', 'SD (Paket A)', '1964-06-11', 4, 'SBU Pengelolaan Air', 14, '2003-03-25', '2003-03-25', '2018-02-15'),
('1410280580', 'HAPIZ', 'SD', '1980-05-25', 4, 'Divisi Keamanan', 11, '2003-03-25', '2003-03-25', '2017-04-28'),
('1410290576', 'MOHAMMAD ALI SAID', 'Madrasah Ibtidaiyah', '1976-05-09', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410300670', 'SABUR', 'SD', '1970-06-25', 4, 'Divisi Keamanan', 11, '2003-03-25', '2003-03-25', '2017-04-28'),
('1410310174', 'NANANG ZUNAIDI', 'Madrasah Ibtidaiyah', '1974-01-27', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410320378', 'WARYONO', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1978-03-23', 4, 'SBU Pelayanan Logistik', 13, '2007-06-07', '2007-06-07', '2014-01-01'),
('1410330471', 'OSENG SUDRAJAT', 'SD', '1971-04-27', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410351263', 'MARULOH', 'SD Tidak Berijazah', '1963-12-12', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2018-08-14'),
('1410360664', 'DARUS SALAM', 'SD Tidak Berijazah', '1964-06-15', 4, 'SBU Pusat Logistik Berikat', 16, '2003-03-25', '2003-03-25', '2018-08-14'),
('1410380976', 'TASIM', 'SD Tidak Berijazah', '1976-09-04', 4, 'SBU Pusat Logistik Berikat', 16, '2003-03-25', '2003-03-25', '2019-05-10'),
('1410390770', 'SANAN', 'SD Tidak Berijazah', '1970-07-03', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410410569', 'SAYIDI', 'SD Tidak Berijazah', '1969-05-20', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('1410420870', 'UJANG BUDI', 'SD Tidak Berijazah', '1970-08-07', 4, 'Divisi Keamanan', 11, '2003-03-25', '2003-03-25', '2018-08-14'),
('1410430583', 'ADE SUDIRMAN', 'Madrasah Ibtidaiyah', '1983-05-05', 4, 'SBU Pengelolaan Air', 14, '2003-03-25', '2003-03-25', '2018-02-15'),
('1410471177', 'ANDI MIHARJA', 'SMA Ilmu-Ilmu Fisik', '1977-11-30', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410480571', 'WAHYUDIN', 'SMA Ilmu-Ilmu Sosial', '1971-05-18', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410490381', 'ABDUL MALIK', 'SMA Ilmu Pengetahuan Sosial', '1981-03-14', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410500870', 'SOBIRIN', 'SMA Ilmu Ilmu Biologi', '1970-08-16', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2018-11-07'),
('1410510970', 'SUMADYO', 'STM Mesin Tenaga', '1970-09-01', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410520480', 'SAAD', 'SMA Ilmu Pengetahuan Sosial', '1980-04-09', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410531272', 'SUSANTO', 'SMA Ilmu-Ilmu Sosial', '1972-12-12', 4, 'Divisi Keamanan', 11, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410541085', 'RIDWAN IDRIS, S.ST', 'D4 Ketatalaksanaan Angkutan Laut dan Kepelabuhanan', '1985-10-23', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410550190', 'FAJAR WAHYU DWI SETIANTO, SH', 'S1 Ilmu Hukum', '1990-01-09', 4, 'Sumber Daya Manusia & Umum', 6, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410560285', 'CHANDRA ANDRIAWAN, ST', 'S1 Teknik Industri', '1985-02-06', 4, 'SBU Prima Beton', 15, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410570589', 'KIKI RIZKIAH', 'SMK Akuntansi', '1989-05-28', 4, 'Sumber Daya Manusia & Umum', 6, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410581087', 'CHARLY MARUDUT', 'D3 Manajemen Informatika', '1987-10-23', 4, 'Pemasaran & Pelayanan', 4, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410590591', 'RICY HERMANTO, SH', 'S1 Ilmu Hukum', '1991-05-07', 4, 'Sumber Daya Manusia & Umum', 6, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410600778', 'YULIA ISMIYATI, SE', 'S1 Manajemen Keuangan', '1978-07-03', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2014-06-02', '2014-06-02', '2014-06-02'),
('1410610392', 'HANNY AULIA TRIASITA, S.Des', 'S1 Desain Komunikasi Visual', '1992-03-05', 4, 'Bagian Sistem Manajemen Informasi', 5, '2014-06-02', '2014-06-02', '2016-11-23'),
('1410621177', 'ANDRI BUDI DARMA', 'D3 Akuntansi', '1977-11-11', 4, 'SBU Prima Beton', 15, '2014-06-02', '2014-06-02', '2019-06-21'),
('1410630488', 'NICOLAS TARIGANTUA, SE', 'S1 Manajemen Perusahaan', '1988-04-03', 4, 'Pemasaran & Pelayanan', 4, '2014-06-09', '2014-06-09', '2019-04-16'),
('1410640289', 'Taudita, ST', 'S1 Teknik Industri', '1989-02-25', 4, 'Perencanaan & Pengawasan', 3, '2014-06-25', '2014-06-25', '2014-06-25'),
('1410650391', 'MERIZA FIRDAYANTI, S.Kom', 'S1 Sistem Komputer', '1991-03-01', 4, 'Sumber Daya Manusia & Umum', 6, '2014-07-01', '2014-07-01', '2014-07-01'),
('1410670583', 'SARI APRILLIA, SE', 'S1 Akuntansi', '1983-05-01', 4, 'Akuntansi & Keuangan', 7, '2013-12-02', '2013-12-02', '2013-12-02'),
('1410720489', 'ANDI PROGO, S.I.Kom', 'S1 Ilmu Komunikasi', '1989-04-21', 4, 'SBU Pusat Logistik Berikat', 16, '2014-12-01', '2014-12-01', '2019-08-22'),
('1410730992', 'AMANDA SEVRILLIA, SM.', 'S1 Manajemen Bisnis Telekomunikasi & Informatika', '1992-09-02', 4, 'Akuntansi & Keuangan', 7, '2015-01-06', '2015-01-06', '2015-01-06'),
('1410751282', 'DESI HERDIANTI', 'D3 Akuntansi', '1982-12-30', 3, 'Pjs. Supervisor Akuntansi & Keuangan', 16, '2014-12-01', '0000-00-00', '0000-00-00'),
('1410760380', 'WAHIDIN', 'SMA Ilmu Pengetahuan Alam (Paket C)', '1980-03-05', 4, 'Sumber Daya Manusia & Umum', 6, '2014-12-01', '2014-12-01', '2014-12-01'),
('1410770585', 'IKHSAN', 'SMK Sekretaris', '1985-05-25', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2014-12-01', '2014-12-01', '2014-12-01'),
('1410780893', 'IBRAHIM', 'SMK Teknik Pengelasan', '1993-08-29', 4, 'SBU Kawasan Cakung', 11, '2014-12-01', '2014-12-01', '2017-11-03'),
('1410790475', 'MAHFUDIN', 'SMEA Perdagangan', '1975-10-08', 4, 'Sumber Daya Manusia & Umum', 6, '2014-12-01', '2014-12-01', '2014-12-01'),
('1410800192', 'ASDAR', 'SMA Bahasa', '1992-01-26', 4, 'Akuntansi & Keuangan', 7, '2014-12-01', '2014-12-01', '2014-12-01'),
('1410810186', 'SYARIFUDDIN SIMANJUNTAK', 'SMK Teknik Mekanik Otomotif', '1986-01-06', 4, 'SBU Pelayanan Logistik', 13, '2014-12-01', '2014-12-01', '2019-07-12'),
('1410820875', 'MARSONO', 'SMK Teknik Mekanik Otomotif', '1975-08-12', 4, 'Sumber Daya Manusia & Umum', 6, '2014-12-01', '2014-12-01', '2014-12-01'),
('149540791', 'PARAMITHA RIZKY KARTIKASARI, ST', 'S1 Teknik Sipil', '1991-07-05', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2014-01-02', '2014-01-02', '2018-10-29'),
('149550288', 'S. FACHRURRAZY ZM. AL AIDID, ST', 'S1 Teknik Arsitektur', '1988-02-16', 4, 'SBU Kawasan Cakung', 11, '2014-01-02', '2014-01-02', '2014-01-02'),
('149561087', 'EVY ELFRIDA, S.Sos', 'S1 Hubungan Internasional', '1987-10-17', 4, 'SBU Pusat Logistik Berikat', 16, '2014-01-02', '2014-01-02', '2019-07-12'),
('149570488', 'Nusin, S.I.Kom', 'S1 Ilmu Komunikasi', '1988-04-27', 4, 'Sekretariat Perusahaan', 1, '2014-01-02', '2014-01-02', '2014-01-02'),
('149630680', 'MAMAN SUWANDI', 'SMA Ilmu Pengetahuan Sosial', '1980-06-03', 4, 'Divisi Keamanan', 11, '2014-01-06', '2014-01-06', '2014-01-06'),
('149640378', 'KARNADI', 'SMA Ilmu Pengetahuan Sosial', '1978-03-01', 4, 'Divisi Keamanan', 11, '2014-01-06', '2014-01-06', '2014-01-06'),
('149650485', 'HUSNUL HAYAT', 'D3 Manajemen Informatika', '1985-04-18', 4, 'Bagian Sistem Manajemen Informasi', 5, '2014-01-06', '2014-01-06', '2018-10-29'),
('149660879', 'TAUFIK FAUZI', 'SMK Teknologi Pengerjaan Logam', '1979-08-07', 4, 'Divisi Keamanan', 12, '2014-01-06', '2014-01-06', '2014-01-06'),
('149671182', 'SUDARMANTO', 'SMK Teknik Mekanik Otomotif', '1982-11-13', 4, 'Divisi Keamanan', 11, '2014-01-06', '2014-01-06', '2014-01-06'),
('149681287', 'Gito', 'SMK Penjualan', '1987-12-25', 4, 'Sekretariat Perusahaan', 1, '2014-01-06', '2014-01-06', '2015-10-15'),
('149691188', 'TAUFIK AKBAR', 'SMK Teknik Mekanik Otomotif', '1988-11-22', 4, 'Program Kemitraan & Bina Lingkungan', 8, '2014-01-06', '2014-01-06', '2019-04-16'),
('149701284', 'AMINUDIN', 'MA Ilmu Pengetahuan Sosial', '1984-12-15', 4, 'Divisi Keamanan', 11, '2014-01-06', '2014-01-06', '2014-01-06'),
('149710468', 'BUDIMAN', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1968-04-08', 4, 'Divisi Keamanan', 11, '2014-01-06', '2014-01-06', '2014-01-06'),
('149720682', 'RUDI SETIAWAN', 'SMU Ilmu Pengtahuan Alam', '1982-06-24', 4, 'Divisi Keamanan', 12, '2014-01-06', '2014-01-06', '2014-01-06'),
('149730786', 'JUMAWAL, SE', 'S1 Manajemen Perusahaan', '1986-07-03', 4, 'Keamanan', 10, '2014-01-06', '2014-01-06', '2014-01-06'),
('149740680', 'ROBY SUTANTO', 'SMK Pemasangan & Perbaikan Mesin Kapal', '1980-06-16', 4, 'Divisi Keamanan', 12, '2014-01-06', '2014-01-06', '2014-01-06'),
('149770488', 'MOCHAMAD NASIR', 'D3 Teknik Elektro', '1988-04-20', 4, 'SBU Pengelolaan Air', 14, '2014-03-03', '2014-03-03', '2019-04-16'),
('149780288', 'Atra, ST', 'S1 Teknik Elektro', '1988-02-29', 4, 'Perencanaan & Pengawasan', 3, '2014-03-07', '2014-03-07', '2014-03-07'),
('149800388', 'Iwan', 'SMK Teknik Mesin', '1988-03-19', 4, 'Perencanaan & Pengawasan', 3, '2014-03-03', '2014-03-03', '2014-03-03'),
('149810989', 'TANIA PERMATASARI, SE', 'S1 Akuntansi', '1989-09-18', 4, 'Akuntansi & Keuangan', 7, '2014-03-17', '2014-03-17', '2014-03-17'),
('149821286', 'LANANG AIRLANGGA', 'SMK Nautika Pelayaran Niaga', '1986-12-01', 4, 'SBU Kawasan Cakung', 11, '2014-04-01', '2014-04-01', '2014-04-01'),
('149831069', 'HERI SUWANTO', 'SMA Ilmu-Ilmu Sosial', '1969-10-28', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149851068', 'MOH. SALEH TONDA', 'SMEA Tata Buku', '1968-10-12', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149880875', 'NADI', 'STM Mesin Tenaga', '1975-08-09', 4, 'SBU Pelayanan Logistik', 13, '2007-06-07', '2007-06-07', '2014-01-01'),
('149890375', 'DADANG AFANDI', 'STM Mesin Tenaga', '1975-03-05', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149900574', 'ABAS', 'STM Listrik', '1974-05-13', 4, 'Divisi Keamanan', 12, '2003-03-25', '2003-03-25', '2018-08-14'),
('149910274', 'LUKMANUL HAKIM', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1974-02-15', 4, 'Sumber Daya Manusia & Umum', 6, '2003-03-25', '2003-03-25', '2013-07-01'),
('149920574', 'SIROJUDDIN', 'STM Mesin Tenaga', '1974-05-24', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149930275', 'MIFTAHUDIN', 'STM Automotif', '1975-02-01', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149940469', 'MUHAMMAD SHOHEH', 'SMA Ilmu-Ilmu Sosial', '1969-04-16', 4, 'SBU Pengelolaan Air', 14, '2003-03-25', '2003-03-25', '2018-02-15'),
('149950478', 'SUKRON', 'MA Ilmu Pengetahuan Sosial', '1978-04-20', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149960177', 'TONI EFENDI', 'SMA Ilmu-Ilmu Biologi', '1977-01-12', 4, 'SBU Pelayanan Logistik', 13, '2003-03-25', '2003-03-25', '2014-01-01'),
('149970587', 'AHMAD ZULFIKAR', 'SMA Ilmu Pengetahuan Sosial', '1987-05-27', 4, 'SBU Pelayanan Logistik', 13, '2007-06-07', '2007-06-07', '2014-01-01'),
('149980774', 'TOHARUDIN', 'SMT Pertanian', '1974-07-12', 4, 'Divisi Keamanan', 11, '2007-06-07', '2007-06-07', '2019-04-16'),
('149990272', 'ROHILI', 'SMU Ilmu Pengetahuan Sosial', '1972-02-14', 4, 'SBU Pelayanan Logistik', 13, '2006-11-01', '2006-11-01', '2014-01-01'),
('1510841090', 'Rizoc, S.I', 'S1 Ilmu Komunikasi Hubungan Masyarakat', '1990-10-05', 4, 'Sekretariat Perusahaan', 1, '2015-01-12', '2015-01-12', '2019-06-21'),
('1510851288', 'EKO SUPRIATNA', 'D3 Akuntansi', '1988-12-17', 4, 'Divisi Keamanan', 12, '2015-01-12', '2015-01-12', '2019-07-12'),
('1510860187', 'HERO KRISTI SALAWATI, SH', 'S1 Ilmu Hukum', '1987-01-01', 4, 'SBU Kawasan Cakung', 11, '2015-01-12', '2015-01-12', '2018-03-15'),
('1510871086', 'Difa, ST', 'S1 Teknik Mesin', '1986-10-28', 4, 'Perencanaan & Pengawasan', 3, '2015-01-12', '2015-01-12', '2019-04-16'),
('1510881273', 'DEDI PURWANTO, A.Pi', 'D4 Teknologi Pengolahan Hasil Perikanan', '1973-12-05', 4, 'SBU Pusat Logistik Berikat', 16, '2015-01-12', '2015-01-12', '2019-04-16'),
('1510891173', 'MAMIN PERMANA', 'SMA Ilmu-Ilmu Sosial', '1973-11-19', 4, 'Akuntansi & Keuangan', 7, '2015-01-12', '2015-01-12', '2015-12-10'),
('1510900791', 'Dowan, ST', 'S1 Teknik Mesin', '1991-07-03', 3, 'Pejabat Setingkat', 3, '2015-01-12', '2018-10-31', '2018-10-31'),
('1510910785', 'RELY EZERO PARD, ST', 'S1 Teknik Mesin', '1985-07-26', 4, 'SBU Pelayanan Logistik', 13, '2015-01-12', '2015-01-12', '2015-01-12'),
('1510920875', 'ACHMAD GOZALI', 'STM Mesin Tenaga', '1975-08-03', 4, 'SBU Pusat Logistik Berikat', 16, '2015-01-12', '2015-01-12', '2019-07-12'),
('1510930786', 'RIZKI ANDAM ALFAT, S.Kom', 'S1 Teknik Informatika', '1986-07-29', 4, 'Bagian Sistem Manajemen Informasi', 5, '2015-01-12', '2015-01-12', '2015-01-12'),
('1510940383', 'DIAN SANTOSA SUDJANNA, S.Kom', 'S1 Teknik Informatika', '1983-03-14', 3, 'Pjs. Supervisor SDM & Umum', 16, '2015-01-12', '0000-00-00', '2018-08-14'),
('1510951189', 'EDWI RAHAYUNINGTYAS, S.Kom', 'S1 Sistem Informasi', '1989-11-14', 4, 'Bagian Sistem Manajemen Informasi', 5, '2015-01-12', '2015-01-12', '2015-01-12'),
('1510960581', 'ENDRO IDIKA UTOMO SOEMENDRO, S.Kom', 'S1 Sistem Informasi', '1981-05-31', 4, 'Manajemen Operasional', 9, '2015-01-12', '2015-01-12', '2015-12-10'),
('1510970389', 'DEDEH MARLINA, SE', 'S1 Akuntansi', '1989-03-07', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2015-01-12', '2015-01-12', '2015-01-12'),
('1510981290', 'TRI WAHYUDI PRASETYO, SH', 'S1 Ilmu Hukum', '1990-12-01', 4, 'Sumber Daya Manusia & Umum', 6, '2015-01-12', '2015-01-12', '2015-01-12'),
('1510990287', 'MOCHAMAD IRFANDI, SE', 'S1 Akuntansi', '1987-02-22', 4, 'SBU Prima Beton', 15, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511000989', 'ROBY WICAKSONO, SE', 'S1 Manajemen Logistik & Material', '1989-09-05', 4, 'SBU Pelayanan Logistik', 13, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511010591', 'FANEMA ANUGERAH, SH', 'S1 Ilmu Hukum', '1991-05-10', 4, 'Sumber Daya Manusia & Umum', 6, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511020485', 'MULYATI, S.IP', 'S1 Ilmu Perpustakaan', '1985-04-29', 4, 'Pemasaran & Pelayanan', 4, '2015-04-06', '2015-04-06', '2019-06-21'),
('1511031275', 'DARYONO', 'STM Mesin Tenaga', '1975-12-30', 4, 'Divisi Keamanan', 11, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511040183', 'YOYON ROHYAN', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1983-01-04', 4, 'SBU Pusat Logistik Berikat', 16, '2015-04-06', '2015-04-06', '2019-09-09'),
('1511050882', 'NAMAN SUPRATMAN', 'SMA Ilmu Pengetahuan Sosial (Paket C)', '1982-08-06', 4, 'Divisi Keamanan', 12, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511061069', 'RUKAEDI', 'SMA Ilmu-Ilmu Sosial', '1969-10-10', 4, 'Divisi Keamanan', 11, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511080170', 'KAMALUDIN', 'SMP', '1970-01-15', 4, 'Divisi Keamanan', 11, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511091170', 'MOCH. AMAL BAKTI', 'ST Teknik Logam', '1970-11-26', 4, 'Divisi Keamanan', 11, '2015-04-06', '2015-04-06', '2015-04-06'),
('1511111286', 'Pupatam', 'D3 Akuntansi', '1986-12-29', 4, 'Satuan Pengawasan Intern', 2, '2015-06-03', '2015-06-03', '2018-08-14'),
('1511130691', 'PRASETIO SABDONO', 'D3 Manajemen Informatika', '1991-06-10', 4, 'Sumber Daya Manusia & Umum', 6, '2015-09-08', '2015-09-08', '2015-12-10'),
('1511140453', 'BJP (Pur) Drs. AGUS SALIM BAKRIE, SH', 'S1 Ilmu Hukum', '1953-04-03', 1, 'Keamanan', 10, '2015-09-01', '2015-09-01', '2015-09-01'),
('1511151090', 'MUHAMAD MUNZIR', 'SMK Teknik Mekanik Otomotif', '1990-10-01', 4, 'SBU Prima Beton', 15, '2015-12-16', '2015-12-16', '2017-10-18'),
('1611160980', 'A. MUH. DATARIANSYAH INDRA HAMZAH, SH., MH.', 'S2 Magister Hukum Perdata', '1980-09-19', 4, 'Sumber Daya Manusia & Umum', 6, '2016-05-02', '2016-05-02', '2016-11-23'),
('1611170591', 'ADHITYA UTAMA, ST', 'S1 Teknik Arsitektur', '1991-05-20', 4, 'SBU Kawasan Cakung', 11, '2016-06-01', '2016-06-01', '2018-08-14'),
('1611181188', 'HARIS HIDAYATULLAH, SE', 'S1 Manajemen', '1988-11-22', 4, 'SBU Kawasan Cakung', 11, '2016-05-02', '2016-05-02', '2019-04-16'),
('1611191189', 'Nutriho, ST', 'S1 Teknik Arsitektur', '1989-11-15', 4, 'Perencanaan & Pengawasan', 3, '2016-05-02', '2016-05-02', '2016-05-02'),
('1611200680', 'Hediyat, ST', 'S1 Teknik Sipil', '1980-06-29', 4, 'Perencanaan & Pengawasan', 3, '2016-05-02', '2016-05-02', '2016-05-02'),
('1611210591', 'PRATIKTO DERMAWAN NUZUL, SE', 'S2 Magister Manajemen', '1991-05-04', 4, 'SBU Pelayanan Logistik', 13, '2016-05-02', '2016-05-02', '2017-04-20'),
('1711221085', 'Irsu', 'SMK Sekretaris', '1985-10-12', 4, 'Sekretariat Perusahaan', 1, '2017-03-01', '2017-03-01', '2017-10-26'),
('1711231292', 'FITRAH', 'SMA Bahasa', '1992-12-20', 4, 'SBU Pusat Logistik Berikat', 16, '2017-03-01', '2017-03-01', '2018-08-14'),
('1711241191', 'Dida, S.I.Kom', 'S1 Ilmu Komunikasi', '1991-11-10', 4, 'Sekretariat Perusahaan', 1, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711250189', 'EDWARD SISWANTO, SE', 'S1 Akuntansi', '1989-01-23', 4, 'Pemasaran & Pelayanan', 4, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711260290', 'FERIYAN CESARUDIN IQBAL, S.Pt', 'S1 Peternakan', '1990-02-17', 4, 'SBU Prima Beton', 15, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711270590', 'TRIA LESTARI, SE', 'S1 Agribisnis', '1990-05-16', 4, 'Pemasaran & Pelayanan', 4, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711281189', 'IDA AYU NOVIANTARI, S.I.Kom', 'S1 Ilmu Komunikasi Hubungan Masyarakat', '1989-11-10', 4, 'SBU Pelayanan Logistik', 13, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711290391', 'Deti, S.S', 'S1 Sastra Inggris', '1991-03-21', 4, 'Sekretariat Perusahaan', 1, '2017-04-10', '2017-04-10', '2018-08-14'),
('1711300293', 'SELLY SALYAH HARTATI UTAMI, SE', 'S1 Ilmu Ekonomi Studi Pembangunan', '1993-02-02', 4, 'Pemasaran & Pelayanan', 4, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711310792', 'BASKORO PRIYAMBODO, ST', 'S1 Teknik Arsitektur', '1992-07-04', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2017-04-10', '2017-04-10', '2019-07-12'),
('1711320693', 'FITRIA ANJAR KUSUMAWATI, S.Ds', 'S1 Desain Interior', '1993-06-18', 4, 'SBU Kawasan Cakung', 11, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711331094', 'INAS LUTHFIANA ZUHLA, S.Pd', 'S1 Pendidikan Teknik', '1994-10-29', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2018-08-14'),
('1711340694', 'Dayu, ST', 'S1 Teknik Arsitektur', '1994-06-10', 4, 'Perencanaan & Pengawasan', 3, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711351192', 'ANDRI SETYATO NUGROHO, SE', 'S1 Akuntansi', '1992-11-18', 4, 'Akuntansi & Keuangan', 7, '2017-04-10', '2017-04-10', '2018-10-29'),
('1711370490', 'CYNTHIA JAYANTI, SE', 'S1 Akuntansi', '1990-04-05', 4, 'SBU Kawasan Cakung', 11, '2017-04-10', '2017-04-10', '2017-10-09');
INSERT INTO `pegawai` (`npp`, `nama`, `pendidikan`, `tgl_lahir`, `kd_jns_jbtan`, `jabatan`, `id_unit_kerja`, `mulai_kerja`, `tmt_jbtan`, `tmt_unit_kerja`) VALUES
('1711380693', 'HESTI DINIATI MISBAH, S.Tr', 'D4 Akuntansi', '1993-06-09', 4, 'SBU Kawasan Cakung', 11, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711390592', 'RIRIEN SETYANINGSIH, SE', 'S1 Akuntansi', '1992-05-19', 4, 'SBU Pelayanan Logistik', 13, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711400594', 'ANDI ARIEFAH MAHANINDHA, SE', 'S1 Akuntansi', '1994-05-27', 4, 'Akuntansi & Keuangan', 7, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711410883', 'YANDO ARIEF AGVIANDO NAVISON, S.Kom', 'S1 Sistem Informasi', '1983-08-01', 4, 'Bagian Sistem Manajemen Informasi', 5, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711420989', 'FAIS NURSUAEDI AROFIK, S.Kom', 'S1 Teknik Informatika', '1989-09-16', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2018-08-14'),
('1711431092', 'HARY SISWANTO, S.Kom', 'S1 Teknik Informatika', '1992-10-04', 4, 'SBU Kawasan Cakung', 11, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711440290', 'ANDRIAN WICAKSONO, SH', 'S1 Ilmu Hukum', '1990-02-12', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711450493', 'ANNISA AWALINNA, SH', 'S1 Ilmu Hukum', '1993-04-05', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711461292', 'ARDIYANTI, SH', 'S1 Ilmu Hukum', '1992-12-02', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2018-08-14'),
('1711470191', 'Dianyari, SKM', 'S1 Kesehatan Masyarakat', '1991-01-31', 4, 'Perencanaan & Pengawasan', 3, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711480789', 'ST. ZAIDAH, SE', 'S1 Manajemen Keuangan', '1989-07-01', 4, 'Manajemen Operasional', 9, '2017-04-10', '2017-04-10', '2018-01-09'),
('1711501185', 'PUJI KUSWANTO, ST', 'S1 Teknik Mesin', '1985-11-24', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2018-03-29'),
('1711510491', 'Munus, ST', 'S1 Teknik Kelautan', '1991-04-26', 4, 'Perencanaan & Pengawasan', 3, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711521093', 'ACI ATIKAH, S.Si', 'S1 Kimia', '1993-10-09', 4, 'SBU Pengelolaan Air', 14, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711531293', 'TRI WAHYU ANDRIYANTO, S.Tr', 'D4 Teknik Informatika', '1993-12-31', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711540588', 'ANDRI FERDIANSYAH', 'D3 Teknik Sipil', '1988-05-14', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2017-04-10', '2017-04-10', '2017-10-09'),
('1711550694', 'FAHMY PERDANA LEKSANA PUTRA', 'D3 Manajemen informatika', '1994-06-15', 4, 'Bagian Sistem Manajemen Informasi', 5, '2017-04-10', '2017-04-10', '2017-04-10'),
('1711560382', 'HENDRY MARZAL SA\'ADI', 'SMU Ilmu Pengetahuan Sosial', '1982-03-26', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2019-08-22'),
('1711571188', 'ANGGA NOVA SETIYAWAN', 'SMA Ilmu Pengetahuan Alam', '1988-11-20', 4, 'SBU Pusat Logistik Berikat', 16, '2017-04-10', '2017-04-10', '2018-08-14'),
('1711580483', 'MUHAMAT FAHROZI', 'SMK Teknik Mesin Perkakas', '1983-04-10', 4, 'Sumber Daya Manusia & Umum', 6, '2017-04-10', '2017-04-10', '2017-10-09'),
('1811600292', 'ULFA FEBRYANTI ZAIN, SH., LL.M.', 'S2 International Business and Corporate Law', '1992-02-14', 4, 'Sumber Daya Manusia & Umum', 6, '2018-02-01', '2018-02-01', '2018-02-01'),
('1811610994', 'BUNGA RAHMAH SAFHIRA, SE', 'S1 Akuntansi', '1994-09-02', 4, 'Akuntansi & Keuangan', 7, '2018-02-01', '2018-02-01', '2018-02-01'),
('1811620783', 'BOBBY MUHAMAD', 'D3 Perhotelan', '1983-07-17', 4, 'Sumber Daya Manusia & Umum', 6, '2018-02-01', '2018-02-01', '2018-02-01'),
('1811631194', 'ILHAM AKBAR', 'SMK Teknik Otomotif Sepeda Motor', '1994-11-07', 4, 'Sumber Daya Manusia & Umum', 6, '2019-04-11', '2019-04-11', '2019-04-11'),
('1811640688', 'REZZY, SH., LL.M.Gen', 'S2 International Law', '1988-06-22', 4, 'Sumber Daya Manusia & Umum', 6, '2018-02-05', '2018-02-05', '2018-02-05'),
('1811650690', 'RIZKI DHARMA DJUMAING, SE', 'S1 Manajemen', '1990-06-26', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '2018-02-05', '2018-02-05', '2018-02-05'),
('1811660694', 'ANDINI HAYRUNNISYAH, S.ST', 'D4 Akuntansi Manajerial', '1994-06-27', 4, 'Akuntansi & Keuangan', 7, '2018-02-05', '2018-02-05', '2018-02-05'),
('1811671192', 'MUH. ILHAM', 'SMA Ilmu Pengetahuan Sosial', '1992-11-10', 4, 'SBU Pelayanan Logistik', 13, '2018-02-05', '2018-02-05', '2018-02-05'),
('1911710792', 'REBERTA YASIN TEGARLAH, ST', 'S1 Teknik Informatika', '1992-07-09', 4, 'Bagian Sistem Manajemen Informasi', 5, '2019-09-01', '2019-09-01', '2019-09-01'),
('1911720396', 'DIANA NURMALASARI, ST', 'S1 Teknik Informatika', '1996-03-02', 4, 'Bagian Sistem Manajemen Informasi', 5, '2019-09-01', '2019-09-01', '2019-09-01'),
('1911731089', 'MUHAMAD RIFQI, M.A', 'S2 Ilmu Administrasi', '1989-10-23', 4, 'Bagian Sistem Manajemen Informasi', 5, '2019-09-01', '2019-09-01', '2019-09-01'),
('1911740793', 'RIZKI ADI PRATOMO, S.Kom', 'S1 Sistem Informasi', '1993-07-05', 4, 'Bagian Sistem Manajemen Informasi', 5, '2019-09-01', '2019-09-01', '2019-09-01'),
('27030476', 'NANANG SURYADI', 'STM Mesin Tenaga', '1976-04-21', 3, 'Umum', 6, '2000-04-01', '2018-03-15', '2010-11-10'),
('27041177', 'HARNANTO BAYU AJI', 'SMA Ilmu-Ilmu Sosial', '1977-11-05', 4, 'Manajemen Operasional', 9, '2000-04-01', '2000-04-01', '2013-01-14'),
('57121165', 'EKO R. KUNTADI', 'D3 Manajemen Informatika', '1965-11-28', 2, 'Sistem Manajemen Informasi', 5, '2004-05-06', '2016-11-23', '2019-06-21'),
('57131276', 'TEGUH IMAN SANTOSO', 'D3 Manajemen Informatika', '1976-12-22', 3, 'Pejabat Setingkat', 9, '2004-05-06', '2016-11-23', '2013-01-14'),
('6771173', 'ACHMAD MAWARDI, SH', 'S1 Ilmu Hukum', '1973-11-10', 2, 'Hukum', 6, '1997-12-17', '2018-01-19', '2018-01-19'),
('6791074', 'EKA KADARUSMAN, SE', 'S1 Manajemen Perusahaan', '1974-10-07', 3, 'Peraturan & Tata Laksana', 6, '1997-12-17', '2013-07-05', '2013-08-30'),
('6850276', 'ERWIN HIDAYAT, SE', 'S1 Manajemen Perusahaan', '1976-02-15', 3, 'Pjs. Pengadaan Barang & Jasa', 6, '1998-09-17', '2019-06-21', '2017-11-03'),
('6910175', 'RAHMANSYAH, SE', 'S1 Manajemen Perusahaan', '1975-01-01', 3, 'Penagihan', 8, '1998-09-17', '2018-10-31', '2016-07-01'),
('6920977', 'SUPRIYONO, SH', 'S1 Ilmu Hukum', '1977-09-20', 3, 'Pertanahan', 6, '1998-09-17', '2016-11-23', '2011-03-04'),
('6961075', 'Lusser, ST', 'S1 Teknik Sipil', '1975-10-25', 1, 'Perencanaan & Pengawasan', 3, '1999-06-21', '2018-01-19', '2011-12-29'),
('871500964', 'MUHAMAD SYAFI\'IE', 'SMEA Tata Buku', '1964-09-05', 3, 'Pajak & Asuransi', 7, '1986-07-01', '2009-05-05', '2003-08-29'),
('883131264', 'Milti, SH', 'S1 Ilmu Hukum', '1964-12-31', 2, 'Administrasi SPI', 2, '1987-05-01', '2012-10-18', '2013-01-14'),
('903160865', 'SALYO, SE', 'S1 Manajemen Perusahaan', '1965-08-15', 3, 'Ketertiban', 12, '1988-08-01', '2012-01-13', '2017-03-08'),
('903181264', 'TJASRUBI A.S., ST', 'S1 Teknik Mesin', '1964-12-12', 3, 'Pejabat Setingkat', 6, '1988-08-01', '2009-05-05', '2018-01-19'),
('913560464', 'Ifa, SE', 'S1 Manajemen Perusahaan', '1964-04-05', 3, 'Perencanaan SPI', 2, '1985-09-01', '1996-07-01', '2018-08-14'),
('913571265', 'ROHBINURWATI, SE', 'S1 Manajemen', '1965-12-18', 1, 'General Manajer SBU Pelayanan Logistik', 13, '1985-11-11', '2017-03-16', '2017-03-16'),
('913630265', 'SUHARNA', 'SD', '1965-02-06', 4, 'Divisi Keamanan', 12, '1987-06-08', '1987-06-08', '2011-04-01'),
('913680665', 'TUANI LUMBAN GAOL', 'SMA IPS ', '1965-06-16', 4, 'SBU Pengelolaan Air', 14, '1987-04-01', '1987-04-01', '2008-05-12'),
('923971267', 'Ojack Riyanto Butar Butar, SE', 'S1 Manajemen Perusahaan', '1967-12-29', 3, 'Pengarsipan', 1, '1988-03-07', '2012-01-13', '2013-07-05'),
('924010665', 'SUYANTO', 'SMP', '1965-06-11', 4, 'SBU Pengelolaan Air', 14, '1988-11-01', '1988-11-01', '2008-05-12'),
('924230765', 'SUPARTA', 'SD', '1965-07-03', 4, 'Divisi Keamanan', 12, '1986-10-29', '1986-10-29', '2019-04-16'),
('944300766', 'KRISNA RYJANAKA, ST', 'S1 Teknik Sipil', '1966-07-26', 2, 'Manajer Kepelabuhanan', 12, '1990-01-08', '2015-07-24', '2018-07-31'),
('944311165', 'Noti, PIA', 'D3 Teknik Manajemen Industri', '1965-11-17', 2, 'Pjs. Auditor Madya', 2, '1990-08-13', '2019-05-10', '2012-08-10'),
('944341166', 'CARWAN, SE', 'S1 Manajemen Perusahaan', '1966-11-07', 3, 'Supervisor Informasi Operasi', 13, '1989-09-01', '2012-01-13', '2018-08-14'),
('944350270', 'CORINA ELAWINI KUPPA, SE', 'S1 Manajemen Perusahaan', '1970-02-09', 3, 'Administrasi Keuangan', 8, '1990-06-01', '2012-01-13', '2013-07-05'),
('944360270', 'Muwan', 'SMA Ilmu-Ilmu Sosial', '1970-02-16', 3, 'Pjs. Auditor Muda', 2, '1990-06-01', '2019-06-21', '2016-11-23'),
('944371066', 'LILI ROHILI, SE', 'S1 Manajemen', '1966-10-09', 3, 'Supervisor Personalia & Umum', 12, '1990-06-08', '2009-05-05', '2016-11-23'),
('944380770', 'KARYANI, SE', 'S1 Manajemen Perusahaan', '1970-07-11', 2, 'Keamanan Kawasan Cakung', 11, '1990-01-10', '2013-07-05', '2013-09-11'),
('944391267', 'IING AHMAD SUJAI, SE', 'S1 Manajemen Perusahaan', '1967-12-15', 3, 'Keamanan Fisik & PMK', 12, '1990-01-10', '2007-09-10', '2007-09-10'),
('944420766', 'JOJOR DORMIAN K. SIANTURI, SE', 'S1 Manajemen Pemasaran', '1966-07-22', 3, 'Supervisor Keuangan & Pajak', 12, '1990-06-01', '2009-05-05', '2019-06-21'),
('944430870', 'Khasi, S.Sos', 'S1 Administrasi Niaga', '1970-08-21', 3, 'Auditor Muda', 2, '1990-01-02', '2009-05-05', '2012-01-13'),
('944501066', 'Yuyu', 'SMA IPA', '1966-10-26', 4, 'Sekretariat Perusahaan', 1, '1990-01-08', '1990-01-08', '2014-01-01'),
('954571065', 'DORKAS SIMANJUNTAK, SH', 'S1 Hukum Keperdataan', '1965-10-01', 3, 'Supervisor Pengelolaan Lingkungan', 11, '1991-03-11', '2009-05-05', '2019-06-21'),
('954601165', 'Rema, SE', 'S1 Ekonomi Manajemen ', '1965-11-28', 1, 'Pemasaran & Pelayanan', 4, '1990-11-13', '2019-01-04', '2019-01-04'),
('954630664', 'YOSEF L.M. SAHULATA, SE', 'S1 Akuntansi', '1964-06-04', 3, 'Supervisor Operasional', 12, '1990-09-10', '2005-07-01', '2012-01-13'),
('954670867', 'AGUS MAHWAN, SE', 'S1 Manajemen Perusahaan', '1967-08-22', 3, 'Supervisor Akuntansi', 13, '1990-12-03', '2016-11-23', '2019-07-12'),
('954720965', 'SAHRI, SE', 'S1 Manajemen Perusahaan', '1965-09-30', 3, 'Supervisor Personalia & Umum', 11, '1991-05-01', '2013-07-05', '2016-11-23'),
('954740269', 'EEN HENDAYANI, SE', 'S1 Manajemen Perusahaan', '1969-02-20', 4, 'SBU Kawasan Cakung', 11, '1990-09-10', '1990-09-10', '2015-12-10'),
('954770666', 'Wati, SE., MM', 'S2 Manajemen Pemasaran', '1966-06-12', 2, 'Pemasaran', 4, '1990-11-01', '2018-01-19', '2018-01-19'),
('954790369', 'Man, SE., MM', 'S2 Manajemen Sumber Daya Manusia', '1969-03-18', 2, 'Manajemen Resiko', 1, '1990-09-10', '2016-11-23', '2014-01-04'),
('954800266', 'AAN ANSORI, SE', 'S1 Manajemen', '1966-02-21', 3, 'Supervisor Pelayanan Perizinan', 11, '1991-03-01', '2010-06-01', '2013-07-05'),
('954810168', 'MILHATUL AENI, SE', 'S1 Manajemen Perusahaan', '1968-01-14', 4, 'SBU Kawasan Cakung', 11, '1991-03-01', '1991-03-01', '2013-07-05'),
('954850965', 'LAMBOK MARUDUT, S.Pd', 'S1 Ekonomi Pendidikan Duania Usaha', '1965-09-23', 3, 'Supervisor Pemeliharaan Kawasan', 12, '1989-03-01', '2011-03-04', '2016-11-23'),
('954870469', 'ROIDA MARIATI TAMBUNAN', 'SMA Ilmu-Ilmu Soaial', '1969-04-06', 3, 'Supervisor Akuntansi', 9, '1990-10-02', '2011-03-04', '2011-07-01'),
('954900264', 'MUJIYONO', 'SMA IPS ', '1964-02-16', 3, 'Komandan Regu Keamanan', 11, '1990-10-02', '1990-10-02', '2008-10-24'),
('954930564', 'BAMBANG PUJI SANTOSO, S.Sos', 'S1 Administrasi Niaga', '1964-05-25', 3, 'Supervisor Pelayanan Pelanggan', 9, '1990-10-02', '2010-06-01', '2018-08-14'),
('954950170', 'EDY SUMANTRI', 'SMP', '1970-01-09', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1991-03-01', '1991-03-01', '2003-03-18'),
('954990969', 'M. THAMRIN', 'SMA', '1969-09-19', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1988-11-01', '1988-11-01', '2012-05-10'),
('955001063', 'PONIMIN', 'SD', '1963-10-27', 4, 'SBU Kawasan Cakung', 11, '1991-01-01', '1991-01-01', '2017-02-01'),
('955060866', 'SUPARNO, S.Ap', 'S1 Administrasi Perpajakan', '1966-08-14', 2, 'Manajer Pelayanan Industri & Umum Personalia', 12, '1995-06-07', '2011-12-29', '2019-05-10'),
('955070368', 'DASEP TITOF, ST., M.Ak., MH., MM., PIA', 'S2 Magister Akuntansi', '1968-03-22', 1, 'General Manajer SBU Kawasan Marunda & Tg.Priok', 12, '1995-06-07', '2015-11-16', '2017-02-01'),
('955090668', 'DJOKO TRIONO, SE., MM', 'S2 Manajemen Sumber Daya Manusia', '1968-06-16', 2, 'SDM', 6, '1992-12-07', '2010-06-01', '2016-07-01'),
('955100266', 'Drs. Wir', 'S1 Ilmu Administrasi', '1966-02-14', 1, 'Satuan Pengawasan Intern', 2, '1994-01-03', '2017-08-14', '2019-07-12'),
('955120270', 'ZAKIYAH MURDIATI, SE', 'S1 Manajemen', '1970-02-11', 3, 'Supervisor Personalia & Umum', 9, '1990-11-01', '2010-06-01', '2010-05-26'),
('955161166', 'SATRIYO WAHYU UTOMO', 'SMA IPS ', '1966-11-26', 4, 'SBU Pelayanan Logistik', 13, '1993-10-01', '1993-10-01', '2006-02-22'),
('955180370', 'Syadilah Ramdani Siregar, SE', 'S1 Manajemen', '1970-03-25', 3, 'Pejabat', 1, '1992-12-07', '2009-05-05', '2018-08-14'),
('955191165', 'Kopri', 'SMP', '1965-11-11', 4, 'Sekretariat Perusahaan', 1, '1991-10-01', '1991-10-01', '2003-03-18'),
('955210864', 'CARMIJI', 'SD Tidak Berijazah', '1964-08-28', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1991-10-01', '1991-10-01', '2015-12-10'),
('955230570', 'MERITA SARI, SE', 'S1 Manajemen Perusahaan', '1970-05-23', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1993-07-01', '1993-07-01', '2019-02-12'),
('955260365', 'RAHMAT GOTRO BASUKI', 'SMA IPS ', '1965-03-15', 4, 'SBU Kawasan Cakung', 11, '1993-07-01', '1993-07-01', '2006-02-22'),
('955270671', 'Salman, ST', 'S1 Teknik Elektro', '1971-06-05', 2, 'Pembangunan', 3, '1993-07-01', '2019-01-04', '2012-01-13'),
('955280668', 'No', 'SMT Perkapalan', '1968-06-16', 3, 'Pelaporan SPI', 2, '1993-07-01', '2011-03-04', '2018-08-14'),
('955300871', 'WAHYUDI', 'SMA Ilmu-Ilmu Sosial', '1971-08-14', 3, 'Supervisor Penagihan', 13, '1993-07-01', '2011-03-04', '2017-08-23'),
('955350766', 'MOCH. MALIK', 'D3 Perpajakan', '1966-07-11', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1994-10-01', '2009-05-05', '2016-11-23'),
('955360271', 'Khabar, SH', 'S1 Ilmu Hukum', '1971-02-01', 2, 'Pengelolaan Lingkungan', 3, '1994-07-01', '2019-01-04', '2019-01-04'),
('955390372', 'UNTUNG', 'SMA Ilmu-Ilmu Sosial', '1972-03-02', 3, 'Supervisor Personalia', 13, '1994-08-01', '2018-03-15', '2018-03-15'),
('955401270', 'TARWAN SUTARWAN, SE', 'S1 Manajemen Perusahaan', '1970-12-30', 3, 'Supervisor Operasional Angkutan', 13, '1994-09-01', '2016-11-23', '2018-01-19'),
('955530966', 'DR. G. A. GUNADI, SH., MH', 'S3 Ilmu Hukum', '1966-09-11', 1, 'Sumber Daya Manusia & Umum', 6, '1992-03-04', '2014-10-01', '2014-10-01'),
('955540968', 'R. GAMBIRO, SE', 'S1 Manajemen Perusahaan', '1968-09-21', 2, 'Keuangan', 7, '1993-05-01', '2017-08-14', '2017-08-14'),
('955551265', 'Drs. Numat', 'S1 Adminitrasi Negara (Sospol)', '1965-12-13', 2, 'Manajemen Pelayanan', 4, '1993-05-01', '2015-12-16', '2015-12-16'),
('955581267', 'ARIO TIDAR', 'D3 Keuangan & Perbankan', '1967-12-19', 2, 'Manajer SBU Pusat Logistik Berikat', 16, '1993-05-01', '2013-07-05', '2018-07-31'),
('955590664', 'Pipari, SE., MM., Pia', 'S2 Manajemen Sumber Daya Manusia', '1964-06-14', 1, 'Pejabat Setingkat', 4, '1993-05-01', '2010-06-25', '2018-02-12'),
('955610768', 'POPPY PURBASARY, SH., M.Kn', 'S2 Magister Kenotariatan', '1968-07-18', 2, 'Manajer Pelayanan Industri & Umum Personalia', 11, '1993-05-01', '2010-06-25', '2016-07-01'),
('955621263', 'Drs. ISKANDAR Z.', 'S1 Akuntansi', '1963-12-24', 2, 'Pejabat Setingkat', 9, '1994-01-03', '2011-12-29', '2013-07-05'),
('955640671', 'KATMINI, SE', 'S1 Manajemen Perusahaan', '1971-06-13', 3, 'Supervisor Akuntansi & Keuangan', 15, '1992-05-01', '2012-01-13', '2019-07-12'),
('955650172', 'SRI AYU RADHAWATI, SE', 'S1 Manajemen Perusahaan', '1972-01-27', 4, 'Manajemen Operasional', 9, '1992-05-01', '1992-05-01', '2015-12-10'),
('955670268', 'PAIMUN, SE', 'S1 Manajemen Perusahaan', '1968-02-21', 2, 'Utilitas', 9, '1992-02-03', '2019-01-04', '2013-01-14'),
('955710566', 'NJAMIJO', 'SMU Ilmu-Ilmu Sosial', '1966-05-20', 4, 'SBU Pelayanan Logistik', 13, '1992-02-03', '1992-02-03', '2003-03-18'),
('955721068', 'SARKOWI', 'SD', '1968-10-05', 4, 'Sumber Daya Manusia & Umum', 6, '1993-01-02', '1993-01-02', '2011-04-01'),
('955740569', 'Lud, SE', 'S1 Manajemen Perusahaan', '1969-05-17', 3, 'Auditor Muda', 2, '1992-02-03', '2009-05-05', '2018-08-14'),
('955761163', 'IRWANTO, S.Ip', 'S1 Ilmu Administrasi Negara (Sospol)', '1963-11-07', 3, 'Supervisor Administrasi & Umum', 14, '1992-02-03', '2009-05-05', '2018-08-14'),
('955771066', 'HERLINA IBRAHIM, S.Ip', 'S1 Ilmu Administrasi Negara (Sospol)', '1966-10-30', 2, 'Pejabat Setingkat', 6, '1992-05-01', '2010-06-01', '2019-06-28'),
('955781268', 'KARSONO', 'SMA Ilmu-Ilmu Biologi', '1968-12-12', 3, 'Perangkat Keras', 5, '1992-01-02', '2012-01-13', '2013-01-14'),
('955790566', 'SUPRAPTO, SE., PIA', 'S1 Akuntansi', '1966-05-06', 2, 'Akuntansi', 7, '1992-02-03', '2010-06-01', '2018-01-19'),
('955800768', 'TAUFIK ', 'SMA IPS ', '1968-07-04', 4, 'Komandan Regu Keamanan', 12, '1992-01-02', '1992-01-02', '2011-04-01'),
('955810867', 'HALIM RUSTANDI', 'SMA Ilmu-Ilmu Sosial', '1967-08-09', 4, 'SBU Pengelolaan Air', 14, '1992-01-02', '1992-01-02', '2012-05-10'),
('955820566', 'YUSUF WALUYO', 'STM Mesin Tenaga', '1966-05-18', 4, 'SBU Pengelolaan Air', 14, '1992-01-02', '1992-01-02', '2016-04-01'),
('955830565', 'Yoto, SE., QIA', 'S1 Manajemen Perusahaan', '1965-05-01', 3, 'Auditor Muda', 2, '1992-01-02', '2018-10-31', '2010-02-23'),
('955840265', 'Sutar, SE', 'S1 Manajemen Perusahaan', '1965-02-22', 3, 'Pelayanan Investasi', 4, '1992-10-01', '2012-01-13', '2013-07-05'),
('955850264', 'Farhan Maulana, SE', 'S1 Manajemen', '1964-02-26', 2, 'Humas & Skretariat', 1, '1992-10-01', '2018-01-19', '2010-06-01'),
('955871067', 'M. TAUFIK', 'SMA IPS ', '1967-10-07', 4, 'SBU Pelayanan Logistik', 13, '1992-10-01', '1992-10-01', '2012-01-16'),
('955880267', 'DAHLAN', 'SMA Imu-Ilmu Biologi', '1967-02-03', 3, 'PMK', 11, '1992-10-01', '2013-07-05', '2017-03-08'),
('955891070', 'SALADINSYAH', 'D3 Akuntansi', '1970-10-09', 3, 'Verifikasi', 7, '1992-11-02', '2009-05-05', '2015-11-12'),
('955910575', 'MARDIYO, SE', 'S1 Manajemen', '1975-05-02', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1994-03-01', '1994-03-01', '2017-07-07'),
('955921267', 'Rita, SH., M.Si', 'S2 Ilmu Adm.& Pengemb. SDM', '1967-12-08', 2, 'Auditro Madya', 2, '1993-05-01', '2009-05-05', '2016-07-01'),
('955941167', 'YATO, SE', 'S1 Akuntansi', '1967-11-27', 2, 'Manajer Akuntansi & Keuangan', 13, '1993-10-01', '2015-12-16', '2017-08-24'),
('956040972', 'NUROCHIM', 'SMU IPS', '1972-09-27', 3, 'Pjs. Supervisor Administrasi & Umum', 14, '1993-08-02', '2019-06-21', '2019-06-21'),
('956061265', 'Aldi Maulana Yusuf, SH., MH', 'S2 Magister Ilmu Hukum', '1965-12-03', 2, 'Staf Direktur Pengembangan Bidang Operasional', 1, '1993-05-01', '2010-06-01', '2018-07-31'),
('956070467', 'DAUD ADHI SUHARTO, SH', 'S1 Hukum Keperdataan', '1967-04-18', 3, 'Perjanjian Pemasaran', 6, '1993-05-01', '2018-01-19', '2019-06-21'),
('956090772', 'Tatar, SE', 'S1 Manajemen', '1972-07-04', 2, 'Pjs. Auditor Madya', 2, '1993-08-02', '2019-05-28', '2019-05-28'),
('956100668', 'FAISUL MAULANA YASIN, SE., PIA', 'S1 Manajemen Perusahaan', '1968-06-20', 2, 'Keamanan Kawasan Marunda', 12, '1993-07-01', '2018-01-19', '2019-05-10'),
('956111268', 'R.S Kono', 'SMA IPS', '1968-12-05', 4, 'Satuan Pengawasan Intern', 2, '1993-07-01', '1993-07-01', '2013-07-05'),
('956140668', 'Asep Saktiawan, SE', 'S1 Akuntansi', '1968-06-12', 1, 'Sekretaris Perusahaan', 1, '1993-07-01', '2014-10-01', '2014-10-01'),
('956161064', 'SETIA BUDI', 'SMA IPA ', '1964-10-01', 3, 'Pjs. Supervisor Pemeliharaan Kawasan', 11, '1993-07-01', '2019-06-21', '2012-01-16'),
('956170970', 'Dyla, SE', 'S1 Manajemen Perusahaan', '1970-09-24', 2, 'Pjs. Monitoring & Analis Pasar', 4, '1993-10-01', '2019-05-10', '2019-05-10'),
('956181272', 'YUSRIL DARUS', 'SMEA Keuangan', '1972-12-30', 4, 'Divisi Keamanan', 11, '1993-07-01', '1993-07-01', '2012-01-16'),
('956190766', 'M. DANIL', 'STM Bangunan', '1966-07-15', 4, 'Manajemen Operasional', 9, '1993-07-01', '1993-07-01', '2013-01-14'),
('956211171', 'HARDO DEDALI, SE', 'S1 Manajemen Perusahaan', '1971-11-28', 3, 'Supervisor Pelayanan Perizinan', 12, '1993-07-01', '2018-03-15', '2008-05-12'),
('956220869', 'AGUS DARMAWAN, SE', 'S1 Manajemen Perusahaan', '1969-08-13', 3, 'Supervisor Keuangan & Pajak', 13, '1993-07-01', '2012-01-13', '2015-11-12'),
('956250670', 'ROOSENO, SE', 'S1 Akuntansi', '1970-06-10', 1, 'Akuntansi & Keuangan', 7, '1993-05-01', '2014-09-15', '2014-09-15'),
('956291164', 'WARLI YUNAN', 'SMA IPS ', '1964-11-05', 4, 'Sumber Daya Manusia & Umum', 6, '1993-07-01', '1993-07-01', '2010-04-01'),
('956300366', 'SUTARJO, SE', 'S1 Manajemen Perusahaan', '1966-03-25', 3, 'Supervisor Pengelolaan Lingkungan', 12, '1992-10-01', '2009-11-11', '2013-01-14'),
('956321164', 'MONANG TUA, SE., PIA', 'S1 Manajemen Perusahaan', '1964-11-24', 2, 'Pjs. Manajer Umum', 9, '1992-11-16', '2019-05-10', '2019-05-10'),
('966360364', 'Drs. Salon', 'S1 Pendidikan Teknik Elektro', '1964-03-01', 3, 'Auditor Muda', 2, '1994-10-03', '2009-05-05', '2013-07-05'),
('966371271', 'WAWAN KUSNAWAN, SE', 'S1 Manajemen Perusahaan', '1971-12-20', 2, 'Umum & Pengadaan Barang & Jasa', 6, '1995-09-01', '2013-07-05', '2016-07-01'),
('966381073', 'M. ROMMI BUCHRAM, SE', 'S1 Manajemen Perusahaan', '1973-10-02', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1995-09-01', '1995-09-01', '2017-11-03'),
('966390564', 'MAGGIANA SOEPJATININGSIH', 'Sekolah Menengah Analis Kesehatan', '1964-05-04', 4, 'SBU Kawasan Cakung', 11, '1994-05-02', '1994-05-02', '2018-12-26'),
('966400369', 'PRAYITNO', 'SMA Pengetahuan Budaya', '1969-03-02', 3, 'Keamanan Fisik', 11, '1994-12-27', '2010-06-01', '2018-01-18'),
('966410470', 'MALIMIN', 'STM Mesin Tenaga', '1970-04-03', 3, 'Supervisor Rumah Tangga & Inventaris', 13, '1994-12-27', '2011-03-04', '2018-10-29'),
('966420870', 'TRI SULO', 'STM Mesin Tenaga', '1970-08-31', 4, 'SBU Kawasan Marunda & Tg.Priok', 12, '1994-12-27', '2011-03-04', '2016-11-23'),
('966440569', 'MACHALI, SE', 'S1 Manajemen Perusahaan', '1969-05-10', 3, 'Supervisor Pengusahaan Gudang Tertutup', 13, '1994-12-27', '2013-07-05', '2003-03-18'),
('966471066', 'MOHAMMAD IRAN', 'SMA IPS ', '1966-10-30', 4, 'Divisi Keamanan', 12, '1993-12-01', '1993-12-01', '2007-09-10'),
('966480168', 'HAMIM, SE', 'S1 Manajemen Perusahaan', '1968-01-28', 3, 'Supervisor Operasional', 14, '1993-12-01', '2013-07-05', '2016-11-23'),
('966491069', 'WIYONO', 'SMEA Keuangan', '1969-10-02', 3, 'Supervisor Operasional', 15, '1993-12-01', '2013-07-05', '2013-07-05'),
('966500271', 'MUHAMAD ROKIB', 'SMA IPS ', '1971-02-12', 4, 'SBU Pelayanan Logistik', 13, '1994-02-01', '1994-02-01', '2019-07-12'),
('966510170', 'ADE SAHRONI', 'SMP', '1970-01-13', 4, 'SBU Pelayanan Logistik', 13, '1994-12-27', '1994-12-27', '2003-03-18'),
('966520564', 'Yonno', 'SMA IPS', '1964-05-07', 4, 'Sekretariat Perusahaan', 1, '1994-12-27', '1994-12-27', '2003-08-23'),
('966530370', 'ASMIN KASSIDY', 'STM Mesin Tenaga', '1970-03-26', 4, 'SBU Kawasan Cakung', 11, '1993-12-01', '1993-12-01', '2007-09-10'),
('966561271', 'M. ERWIN ISKANDAR, SE', 'S1 Manajemen Perusahaan', '1971-12-15', 3, 'Supervisor Kebersihan', 9, '1995-09-01', '2012-01-13', '2013-07-05'),
('966570764', 'BUDI IRWANTO, SE', 'S1 Akuntansi', '1964-07-01', 3, 'Supervisor Akuntansi', 11, '1994-12-01', '2009-05-05', '2016-11-23'),
('97190177', 'Dedi, ST', 'S1 Teknik Sipil', '1977-01-28', 2, 'Perencanaan', 3, '2007-09-03', '2019-01-04', '2008-05-12'),
('97201179', 'FITRIYAH SALIM, ST', 'S1 Teknik Informatika', '1979-11-16', 3, 'Perangkat Lunak / Program', 5, '2007-04-02', '2012-01-13', '2015-11-12'),
('97211181', 'ROLLY RAHMANTO, ST', 'S1 Teknik Elektro', '1981-11-19', 3, 'Pembinaan', 8, '2007-04-02', '2016-11-23', '2016-11-23'),
('97221081', 'BETTY ISMAYA SUDJANNA, SE', 'S1 Akuntansi', '1981-10-22', 3, 'Analisa Keuangan', 7, '2007-04-02', '2011-03-04', '2011-03-04'),
('976580869', 'M. S. BUSTOMI, ST', 'S1 Teknik Arsitektur', '1969-08-28', 1, 'Manajemen Operasional', 9, '1996-04-01', '2018-02-12', '2019-07-12'),
('976601273', 'Ryadi', 'STM Listrik', '1973-12-31', 3, 'Pengawasan Bangunan', 3, '1996-11-01', '2018-07-31', '2012-01-16'),
('976610669', 'DEDI', 'D3 Akuntansi', '1969-06-15', 3, 'Treasuri', 7, '1996-11-01', '2012-01-13', '2012-12-14'),
('976620669', 'Tuswan', 'STM Listrik', '1969-06-28', 3, 'Pengawasan Penerbitan IMB', 3, '1996-11-01', '2012-01-13', '2001-01-12'),
('976630267', 'ACEP TAUFIK HIDAYAT', 'SMA IPS', '1967-02-15', 4, 'SBU Pelayanan Logistik', 13, '1996-11-01', '1996-11-01', '2018-10-29'),
('986651266', 'Ir. ABDUL SYUKUR', 'S1 Teknik Sipil', '1966-12-05', 2, 'Manajer Pemeliharaan Kawasan', 11, '1997-02-20', '2018-01-19', '2018-07-31'),
('986660374', 'MUH. ARAS, SE', 'S1 Manajemen Perusahaan', '1974-03-04', 4, 'SBU Kawasan Cakung', 11, '1997-05-07', '1997-05-07', '2018-12-26'),
('986670372', 'BUSJROL FAARIHIN, SE', 'S1 Akuntansi', '1972-03-03', 2, 'Manajer Akuntansi & Keuangan', 11, '1997-08-20', '2015-12-16', '2018-07-31'),
('996690772', 'FITRIANI, SE.Ak', 'S1 Akuntansi', '1972-07-27', 2, 'Program Kemitraan & Bina Lingkungan', 8, '1997-11-17', '2016-11-23', '2016-11-23'),
('996711177', 'ERMU KASUMAWATI, SE', 'S1 Manajemen Perusahaan', '1977-11-16', 4, 'Sumber Daya Manusia & Umum', 6, '1997-12-17', '1997-12-17', '2016-04-01'),
('996720772', 'BASUKI RAHMAD, SE', 'S1 Manajemen Perusahaan', '1972-07-19', 2, 'Manajer Pelayanan Forwarding', 13, '1997-12-17', '2015-07-24', '2018-07-31'),
('996730274', 'JALI, SE', 'S1 Manajemen Perusahaan', '1974-02-02', 4, 'Sumber Daya Manusia & Umum', 6, '1997-12-17', '1997-12-17', '2016-11-23'),
('996740875', 'Gugus, SE', 'S1 Manajemen Perusahaan', '1975-08-12', 4, 'Pemasaran & Pelayanan', 4, '1997-12-17', '1997-12-17', '2016-11-23'),
('996751171', 'Raymana Apprilian', 'D3 Keuangan & Perbankan', '1971-11-27', 3, 'Analisis Resiko Usaha', 1, '1997-12-17', '2019-06-21', '2019-04-16'),
('996760275', 'PIDEL SITOMPUL', 'SMA Ilmu-Ilmu Sosial', '1975-02-23', 4, 'SBU Pelayanan Logistik', 13, '1997-12-17', '1997-12-17', '2015-12-10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_hp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id`, `username`, `password`, `nama`, `no_hp`) VALUES
(1, 'sdm', 'sdm123', 'Janaka', 885552355);

-- --------------------------------------------------------

--
-- Struktur dari tabel `unit_kerja`
--

CREATE TABLE `unit_kerja` (
  `id` int(11) NOT NULL,
  `unit_kerja` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `unit_kerja`
--

INSERT INTO `unit_kerja` (`id`, `unit_kerja`) VALUES
(1, 'Sekretaris Perusahaan'),
(2, 'Satuan Pengawasan Intern'),
(3, 'Perencanaan'),
(4, 'Pemasaran'),
(5, 'Manajemen Informasi'),
(6, 'SDM & Umum'),
(7, 'Akuntansi & Keuangan'),
(8, 'PKBL'),
(9, 'Manajemen Operasional'),
(10, 'Keamanan'),
(11, 'Cakung'),
(12, 'Marunda & Tg. Priok'),
(13, 'UUPL'),
(14, 'SBU Air'),
(15, 'SBU Prima Beton'),
(16, 'SBU PLB');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `jns_jbtan`
--
ALTER TABLE `jns_jbtan`
  ADD PRIMARY KEY (`kd`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`npp`),
  ADD KEY `fk1_pegawai` (`id_unit_kerja`),
  ADD KEY `fk2_pegawai` (`kd_jns_jbtan`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `unit_kerja`
--
ALTER TABLE `unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `fk1_pegawai` FOREIGN KEY (`kd_jns_jbtan`) REFERENCES `jns_jbtan` (`kd`),
  ADD CONSTRAINT `fk2_pegawai` FOREIGN KEY (`id_unit_kerja`) REFERENCES `unit_kerja` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
